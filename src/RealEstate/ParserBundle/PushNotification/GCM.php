<?php 
namespace RealEstate\ParserBundle\PushNotification;

class GCM {
    CONST GCM_URL = 'https://android.googleapis.com/gcm/send';
    
    private $config;
    private $debug;
    private $androidRegIds;



    public function __construct($config, $debug = true)
    {
        $this->config = $config;
        $this->debug = $debug;
    }
    
    public function setRegIds($pushTokenRecords)
    {
        foreach ($pushTokenRecords as $entity)
        {
            if ($entity->getAndroidRegId())
                $this->androidRegIds[] = $entity->getAndroidRegId();
        }
    }
    
    public function sendMessage($message, $data = null)
    {
        if (empty($this->androidRegIds))
            return;
        
        $post = array(
            'registration_ids'  => $this->androidRegIds,
            'data'              => array(
                'message'   => $message,
                'title'	    => 'Приложение Вмигаренда',
                'vibrate'   => 1,
                'sound'	    => 1
            )
        );
        
        if ($data)
            $post['data'] = array_merge($post['data'], $data);
        
        $headers = array(
            'Authorization: key=' . $this->config['api_key'],
            'Content-Type: application/json'
        );
        
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, self::GCM_URL ); // Set URL to GCM endpoint
        curl_setopt( $ch, CURLOPT_POST, true ); // Set request method to POST
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers ); // Set our custom headers
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true ); // Get the response back as string instead of printing it
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $post ) ); // Set post data as JSON
        $result = curl_exec( $ch ); // Actually send the push!
        
        if ($this->debug && curl_errno( $ch ))
            echo 'GCM error: ' . curl_error( $ch );
        
        curl_close($ch);
        
        if ($this->debug)
            echo $result; // debug response
    }
}