<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace RealEstate\ParserBundle\PushNotification;

/**
 * Description of Messenger
 *
 * @author alex
 */
class Messenger {
    //put your code here
    private $GCM;
    private $APN;
    
    
    
    public function __construct($container, $debug = true)
    {
        $gcmConfig = $container->getParameter('gcm_config');
        
        $this->APN = new APN($container, $debug);
        $this->GCM = new GCM($gcmConfig, $debug);
    }
    
    public function setPushTokenRecords($pushTokenRecords)
    {
        $this->APN->setDeviceTokens($pushTokenRecords);
        $this->GCM->setRegIds($pushTokenRecords);
    }
    
    public function sendMessage($message, $data = null)
    {
        $this->APN->sendMessage($message, $data);
        $this->GCM->sendMessage($message, $data);
    }
}
