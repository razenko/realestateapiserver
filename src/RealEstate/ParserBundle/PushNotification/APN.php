<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace RealEstate\ParserBundle\PushNotification;

use Freegli\Component\APNs\Notification;

/**
 * Description of APN
 *
 * @author alex
 */
class APN {
    private $container;
    private $debug;
    private $deviceTokens;
    private $notificationHandler;
    
    
    
    public function __construct($container, $debug = true) {
        $this->container = $container;
        $this->debug = $debug;
        
        $apnConfig = $this->container->getParameter('apn_config');
        $service_profile = $apnConfig['service_profile'];
        
        /*
         * @see app/config/config.yml
         * 
         * freegli.apns.notification_handler.dev -- сервис отправки пуш уведомлений
         * с development сертификатом
         * 
         * freegli.apns.notification_handler.prod -- сервис отправки пуш уведомлений
         * с production сертификатом
         */
        $apns_service = 'freegli.apns.notification_handler.' . $service_profile;
        $this->notificationHandler = $this->container->get($apns_service);  
    }
    
    public function setDeviceTokens($pushTokenRecords)
    {
        foreach ($pushTokenRecords as $entity)
        {
            if ($entity->getAppleDeviceToken())
                $this->deviceTokens[$entity->getUserId()] = $entity->getAppleDeviceToken();
        }
    }
    
    public function sendMessage($message, $data = null)
    {
        if (empty($this->deviceTokens))
            return;
        
        $expireDate = new \DateTime('now');
        $apnConfig = $this->container->getParameter('apn_config');
        $expire_days = $apnConfig['notification_expire_days'];
        $expireDate = $expireDate->add(new \DateInterval('P' . $expire_days . 'D'));
        
        $notification = new Notification();
        $notification->setExpiry($expireDate);
        
        $payload = array(
            'aps' => array(
                'alert' => $message,
                'sound'	=> 'default'
            )
        );
        
        if ($data)
        {
            if (isset($data['badge']))
                $payload['aps']['badge'] = $data['badge'];
            
            unset($data['badge']);
            
            $payload = array_merge($payload, $data);
        }
        
        $notification->setPayload($payload);

        $nh = $this->notificationHandler;
        $result = array();
        $id = 0;

        // send notifications
        foreach ($this->deviceTokens as $user_id => $token)
        {
            $notification->setIdentifier($id);
            $notification->setDeviceToken($token);

            $retry = 2;
            while ($retry)
            {
                try {
                    $result[$user_id] = $nh->send($notification);
                    break;
                } catch (ExceptionInterface $e) {
                    $retry--;
                }
            }
            
            $id++;
        }
        
        $errors = $nh->getErrors();
        
        if ($this->debug && !empty($errors))
            var_dump($errors);
        
        if ($this->debug)
            var_dump($result); // debug response
    }
}
