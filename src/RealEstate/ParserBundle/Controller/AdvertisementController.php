<?php

namespace RealEstate\ParserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use RealEstate\ParserBundle\Controller\ApiController;
use RealEstate\ParserBundle\Entity\UserData;
use RealEstate\ParserBundle\Entity\Realty;
use RealEstate\ParserBundle\Form\Type\RealtyType;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdvertisementController extends Controller
{

    /** @DI\Inject("doctrine.orm.entity_manager") */
    protected $em;

    public function getAction(Request $request)
    {
        $user = ApiController::getUser($request, $this->container);
        $params = ApiController::getRequestParameter('filter', $request);
        $max_advertisements_count = $this->container->getParameter('max_advertisements_count');

        if (!$params)
            return ApiController::getNotFoundError();

        $decoded_params = json_decode($params, true);

        $realties = $this->em->getRepository('RealEstate\ParserBundle\Entity\Realty')
            ->getRealtyByRequestParams($decoded_params, $max_advertisements_count);

        $response = array(
            "advertisements" => array(),
            "photos"         => array());
        
        $realtyIds = array();

        foreach ($realties as $key => $value)
        {
            $realtyIds[$key] = $value->getId();
            
            $response["advertisements"][$key] = array(
                "id"                => $value->getId(),
                "favorite"          => false, // TODO get from user table
                "title"             => $value->getTitle(),
                "term"              => $value->getTerm(),
                "address"           => $value->getAddress(),
                "metro"             => $value->getMetro(),
                "price"             => $value->getPrice(),
                "payment"           => $value->getPayment(),
                "phone_1"           => $value->getContactPhone1(),
                "phone_2"           => $value->getContactPhone2(),
                "description"       => $value->getComment(),
                "furniture"         => $value->getFurniture(),
                "kitchen_furniture" => $value->getKitchenFurniture(),
                "telephone"         => $value->getTelephone(),
                "tv"                => $value->getTv(),
                "refrigerator"      => $value->getRefrigerator(),
                "washing_machine"   => $value->getWashingMachine(),
                "balcony"           => $value->getBalcony(),
                "pets"              => $value->getPets(),
                "children"          => $value->getChildren(),
                "latitude"          => $value->getLatitude(),
                "longitude"         => $value->getLongitude(),
                "updated_at_date"   => $value->getUpdatedAtDate()->format('Y-m-d'),
                "updated_at_time"   => $value->getUpdatedAtTime()->format('H:i:s')
            );
        }
        
        $photos = $this->em->getRepository('RealEstate\ParserBundle\Entity\Realty')
            ->getPhotosByRealtyIds($realtyIds);
        
        if ($photos)
        {
            foreach ($photos as $key => $photo)
            {
                $response["photos"][$key] = array(
                    "id"               => $photo->getId(),
                    "advertisement_id" => $photo->getRealtyId(),
                    "url"              => $photo->getUrl()
                );
            }
        }
        
        if (!empty($response["advertisements"]))
        {
            $date                    = date('Y-m-d H:i:s');
            $response["server_time"] = new \DateTime($date);
        }
        
        $userData = $this->em->getRepository('RealEstateParserBundle:UserData')
            ->findOneById(UserData::REALTY_ID);
        $this->em->getRepository('RealEstateParserBundle:UserDataState')
            ->setChangedState($user, $userData, false);
        
        return ApiController::getDataResponse($response);
    }

    public function changeStateAction(Request $request)
    {
        $user = ApiController::getUser($request, $this->container);
        $advertisement  = ApiController::getRequestParameter('advertisement', $request);
        $advertisement = json_decode($advertisement, true);
        
        if (!$user || empty($advertisement))
            return ApiController::getNotFoundError();
        
        $userRealty = $this->em->getRepository('RealEstate\ParserBundle\Entity\UserRealty')
            ->findOneBy(array('realty_id' => $advertisement['id'], 'user_id' => $user->getId()));
        
        if (!$userRealty)
        {
            $realty =  $this->em->getRepository('RealEstate\ParserBundle\Entity\Realty')
                ->findOneById($advertisement['id']);
            
            $userRealty = new \RealEstate\ParserBundle\Entity\UserRealty();
            $userRealty->setUser($user);
            $userRealty->setUserId($user->getId());
            $userRealty->setRealty($realty);
            $userRealty->setRealtyId($advertisement['id']);
        }
        
        $new_state = !$advertisement['favorite'];
        $userRealty->setIsFavorite($new_state);
        
        $is_new_entity = \Doctrine\ORM\UnitOfWork::STATE_NEW === $this->em->getUnitOfWork()->getEntityState($userRealty);
        
        if ($is_new_entity)
            $this->em->persist($userRealty);
        
        $this->em->flush();
        
        return ApiController::getDataResponse(array());
    }
    
    public function saveAction(Request $request)
    {
        $realtyData = ApiController::getRequestParameter('advertisement', $request);

        if (!$realtyData)
            return ApiController::getNotFoundError();

        $formData = json_decode($realtyData, true);
        $realty = new Realty();

        $form = $this->createForm(new RealtyType(), $realty, array('csrf_protection' => false));
        $form->submit($formData);

        if ($form->isValid())
        {
            $realty->setRegion($realty->getRegionId());
            $realty->setTitle($realty->makeTitle());
            $realty->setFloors('');
            $realty->setPercentage(0);
            $realty->setHash($realty->makeHash());

            $this->em->persist($realty);
            $this->em->flush();

            return ApiController::getDataResponse([]);
        }

        foreach($form->getErrors(true) as $error)
        {
            $raw_field_data = $error->getCause()->getPropertyPath();
            $raw_field_data = str_replace('children[', '', $raw_field_data);
            $field = str_replace(']', '', $raw_field_data);
            $response = array('text' => $field . ' - ' . $error->getMessage());

            return ApiController::getErrorResponse($response);
        }
    }
}
