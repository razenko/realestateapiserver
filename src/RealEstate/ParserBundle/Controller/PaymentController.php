<?php

namespace RealEstate\ParserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use RealEstate\ParserBundle\Controller\ApiController;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use RealEstate\ParserBundle\Entity\User;
use RealEstate\ParserBundle\Model\VariablesLogger\VariablesLogger;
use RealEstate\ParserBundle\Entity\TariffTransaction;
use RealEstate\ParserBundle\Payment\Platron;
use RealEstate\ParserBundle\Payment\Exception\RejectedException;
use RealEstate\ParserBundle\Payment\Exception\ErrorException;
use RealEstate\ParserBundle\PushNotification\Messenger;



class PaymentController extends Controller
{
    /** @DI\Inject("doctrine.orm.entity_manager") */
    protected $em;
    
    private $responseData = array();
    
    
    
    public function checkPaymentAction(Request $request)
    {
        $this->responseData['pg_salt'] = md5(time());
        $script_url = $request->getUri();
        $platronConfig = $this->container->getParameter('platron');

        try
        {
            $order_id = ApiController::getRequestParameter('pg_order_id', $request);
            $pg_sig = ApiController::getRequestParameter('pg_sig', $request);

            $data = ApiController::getRequestParameters($request, $this->container);
            $platron = new Platron($platronConfig, $script_url, $data);

            $timeout = $platronConfig['timeout'];

            if (!$platron->checkSig($pg_sig))
                $this->returnErrorData("rejected", "incorrect sig");

            $tariff_transaction = $this->em
                ->getRepository('RealEstateParserBundle:TariffTransaction')
                ->findOneById($order_id);

            if ($tariff_transaction)
            {
                if ($tariff_transaction->getClosed())
                    $this->returnErrorData("rejected", "transaction is not initialized");

                $this->checkTariffTransaction($tariff_transaction, $data, $timeout);
            }
            else
                $this->returnErrorData("rejected", "transaction is not initialized");

            $this->responseData['pg_status'] = "ok";
            $this->responseData['pg_timeout'] = $timeout;
            $platron = new Platron($platronConfig, $script_url, $this->responseData);
            $this->responseData['pg_sig'] = $platron->generateSig();
            
            return $this->checkOkResponse();
        }
        catch (RejectedException $e)
        {
            $tariff_transaction = isset($tariff_transaction) ? $tariff_transaction : null;
            return $this->handleRejectedException($platronConfig, $script_url, $tariff_transaction);
        }
        catch (\Exception $e)
        {
            $tariff_transaction = isset($tariff_transaction) ? $tariff_transaction : null;
            return $this->handleErrorException($platronConfig, $script_url, $tariff_transaction);
        }
    }

    public function resultPaymentAction(Request $request)
    {
        $this->responseData['pg_salt'] = md5(time());
        $script_url = $request->getUri();
        $platronConfig = $this->container->getParameter('platron');
        $timeout = $platronConfig['timeout'];
        
        try
        {
            $data = ApiController::getRequestParameters($request, $this->container);
            $order_id = $data['pg_order_id'];
            $pg_sig = $data['pg_sig'];
            $pg_result = $data['pg_result'];
            
            $platron = new Platron($platronConfig, $script_url, $data);
            
            if (!$platron->checkSig($pg_sig))
                $this->returnErrorData("rejected", "incorrect sig");
            
            $tariff_transaction = $this->em
                ->getRepository('RealEstateParserBundle:TariffTransaction')
                ->findOneById($order_id);
            
            if ($tariff_transaction && !$tariff_transaction->getClosed())
            {
                if ($pg_result == TariffTransaction::RESULT_FAILED || isset($data['pg_failure_code']))
                {
                    $tariff_transaction->setClosed(true);
                    $tariff_transaction
                        ->setStatus(TariffTransaction::STATUS_FAILED);
                    
                    if (isset($data['pg_failure_code']))
                    {
                        $tariff_transaction
                            ->setErrorCode($data['pg_failure_code']);
                        $tariff_transaction
                            ->setErrorMessage($data['pg_failure_description']);
                    }
                    
                    $this->em->flush();
                    
                    return $this->handleResultSuccess($platronConfig, $script_url, $tariff_transaction);
                }
                
                $this->checkTariffTransaction($tariff_transaction, $data, $timeout);
            }
            elseif (!$tariff_transaction)
                $this->returnErrorData("rejected", "transaction is not initialized");
            
            if (!$tariff_transaction->getClosed())
            {
                $tariff_transaction->setPaymentId($data['pg_payment_id']);
                $tariff_transaction->setStatus(TariffTransaction::STATUS_SUCCESSED);
                $tariff_transaction->setClosed(true);
                
                $this->em->flush();
                
                $this->updateUserSubscription($tariff_transaction);
            }
            
            return $this->handleResultSuccess($platronConfig, $script_url, $tariff_transaction);
        }
        catch (RejectedException $e)
        {
            $tariff_transaction = isset($tariff_transaction) ? $tariff_transaction : null;
            return $this->handleRejectedException($platronConfig, $script_url, $tariff_transaction);
        }
        catch (\Exception $e)
        {
            $tariff_transaction = isset($tariff_transaction) ? $tariff_transaction : null;
            return $this->handleErrorException($platronConfig, $script_url, $tariff_transaction);
        }
    }
    
    public function handleIAPAction(Request $request)
    {
        $user = ApiController::getUser($request, $this->container);
        
        if (!$user)
            return ApiController::getNotFoundError();
        
        $tariff_id = ApiController::getRequestParameter('tariff_id', $request);
        $payment_id = ApiController::getRequestParameter('transaction_id', $request);
        $price = ApiController::getRequestParameter('price', $request);
        
        $price = preg_replace('/[^0-9\.]/', '', $price);
        
        $tariffTransaction = $this->em
            ->getRepository('RealEstateParserBundle:TariffTransaction')
            ->create($user, array(
                'tariff_id' => $tariff_id,
                'paysystem' => TariffTransaction::IOS_IAP_PAYSYSTEM,
                'price' => $price,
                'payment_id' => $payment_id,
                'status' => TariffTransaction::STATUS_SUCCESSED,
                'closed' => true
            ));
        
        if (!$tariffTransaction)
            return ApiController::getServerErrorResponse('Не найден тариф');
        
        $this->updateUserSubscription($tariffTransaction);
        
        return ApiController::getDataResponse(array());
    }
    
    /* 
    * TODO разобраться с генерацией объекта класса через строку
    * классы с namespace не обнаруживаются при таком вызове
    */
    private function returnErrorData($status, $message)
    {
        $this->responseData['pg_status'] = $status;
        $this->responseData['pg_description'] = $message;
        
        switch ($status)
        {
            case 'rejected':
                throw new RejectedException($message);
                break;
            
            case 'error':
                throw new ErrorException($message);
                break;
            
            default:
                throw new \Exception($message);
        }
    }
    
    private function checkTariffTransaction($tariff_transaction, $data, $timeout)
    {
        $currency = $data['pg_currency'];
        $tariff = $tariff_transaction->getTariff();
        
        if (!$tariff_transaction->isInitialized())
            $this->returnErrorData("rejected", "transaction is not initialized");
        
        if (!isset($data['tariff_id']) || $data['tariff_id'] != $tariff->getId())
            $this->returnErrorData("rejected", "wrong tariff");
        
        if ($tariff->getCurrencyKey() != $currency)
            $this->returnErrorData("rejected", "wrong currency");
        
        $updated_time = $tariff_transaction->getUpdatedAt()->format('U');
        
        if ($updated_time + $timeout < time())
            $this->returnErrorData("rejected", 'transaction lifetime ended');
    }
    
    private function handleResultSuccess($platronConfig, $script_url, $tariff_transaction)
    {
        $this->responseData['pg_status'] = "ok";
        $this->responseData['pg_description'] = "transaction completed";
        $platron = new Platron($platronConfig, $script_url, $this->responseData);
        $this->responseData['pg_sig'] = $platron->generateSig();
        
        return $this->resultOkResponse();
    }
    
    private function handleRejectedException($platronConfig, $script_url, $tariff_transaction)
    {
        $platron = new Platron($platronConfig, $script_url, $this->responseData);
        $this->responseData['pg_sig'] = $platron->generateSig();
        
        if ($tariff_transaction)
        {
            $tariff_transaction->setStatus(TariffTransaction::STATUS_REJECTED);
            $tariff_transaction->setClosed(true);
            
            $this->em->flush();
        }
        
        return $this->rejectedResponse();
    }
    
    private function handleErrorException($platronConfig, $script_url, $tariff_transaction)
    {
        $this->responseData['pg_error_description'] = 'server error';
        
        $platron = new Platron($platronConfig, $script_url, $this->responseData);
        $this->responseData['pg_sig'] = $platron->generateSig();
        
        if ($tariff_transaction)
        {
            $tariff_transaction->setStatus(TariffTransaction::STATUS_FAILED);

            $this->em->flush();
        }
        
        return $this->errorResponse();
    }
    
    private function updateUserSubscription($tariffTransaction)
    {
        $this->em
            ->getRepository('RealEstateParserBundle:TariffSubscription')
            ->updateUserTariffSubscription($tariffTransaction);
        
        $pushTokenRecords = $this->em
            ->getRepository('RealEstateParserBundle:MobileReg')
            ->getMobilePushTokensByUserId($tariffTransaction->getUser()->getId());
        
        if (\count($pushTokenRecords))
        {
            $pushNotificationMessenger = new Messenger($this->container, false);
            $pushNotificationMessenger->setPushTokenRecords($pushTokenRecords);

            $data = array('callback' => 'refreshUserData');
            $message = 'Оплата тарифа засчитана';

            $pushNotificationMessenger->sendMessage($message, $data);
        }
    }
    
    private function resultOkResponse()
    {
        $responseData = $this->responseData;
        $response = new Response(
<<<EOX
<?xml version="1.0" encoding="utf-8"?>
<response>
    <pg_salt>{$responseData['pg_salt']}</pg_salt>
    <pg_status>{$responseData['pg_status']}</pg_status>
    <pg_description>{$responseData['pg_description']}</pg_description>
    <pg_sig>{$responseData['pg_sig']}</pg_sig>
</response>
EOX
        );
        
        $response->headers->set('Content-Type', 'text/xml');
        
        return $response;
    }

    private function rejectedResponse()
    {
        $responseData = $this->responseData;
        $response = new Response(
<<<EOX
<?xml version="1.0" encoding="utf-8"?>
<response>
    <pg_salt>{$responseData['pg_salt']}</pg_salt>
    <pg_status>{$responseData['pg_status']}</pg_status>
    <pg_description>{$responseData['pg_description']}</pg_description>
    <pg_sig>{$responseData['pg_sig']}</pg_sig>
</response>
EOX
        );

        $response->headers->set('Content-Type', 'text/xml');
        
        return $response;
    }

    private function checkOkResponse()
    {
        $responseData = $this->responseData;
        $response = new Response(
<<<EOX
<?xml version="1.0" encoding="utf-8"?>
<response>
    <pg_salt>{$responseData['pg_salt']}</pg_salt>
    <pg_status>{$responseData['pg_status']}</pg_status>
    <pg_timeout>{$responseData['pg_timeout']}</pg_timeout>
    <pg_sig>{$responseData['pg_sig']}</pg_sig>
</response>
EOX
        );
        
        $response->headers->set('Content-Type', 'text/xml');
        
        return $response;
    }

    private function errorResponse()
    {
        $responseData = $this->responseData;
        $response = new Response(
<<<EOX
<?xml version="1.0" encoding="utf-8"?>
<response>
    <pg_salt>{$responseData['pg_salt']}</pg_salt>
    <pg_status>error</pg_status>
    <pg_error_description>{$responseData['pg_error_description']}</pg_error_description>
    <pg_sig>{$responseData['pg_sig']}</pg_sig>
</response>
EOX
        );
        
        $response->headers->set('Content-Type', 'text/xml');
        
        return $response;
    }
}