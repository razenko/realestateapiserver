<?php

namespace RealEstate\ParserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use RealEstate\ParserBundle\Controller\ApiController;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use RealEstate\ParserBundle\Entity\Auth;
use RealEstate\ParserBundle\Entity\User;
use RealEstate\ParserBundle\Entity\UserData;
use RealEstate\ParserBundle\Model\SMS\SmsSender;
use RealEstate\ParserBundle\PushNotification\Messenger;
use RealEstate\ParserBundle\Model\VariablesLogger\VariablesLogger;



class UserController extends Controller
{
    /** @DI\Inject("doctrine.orm.entity_manager") */
    protected $em;

    public function sendloginSmsAction(Request $request)
    {
        ApiController::getRequestParameters($request, $this->container);
        $raw_phone = ApiController::getRequestParameter('phone', $request);

        if (!$raw_phone)
        {
            return ApiController::getErrorResponse(array(
                'Phone number is required!'
            ));
        }

        $testUserConfig = $this->container->getParameter('test_user');
        $phone = trim(str_replace("+" , "", $raw_phone));

        if (preg_match('/^' . $testUserConfig['phone_prefix'] . '/', $phone))
            return ApiController::getDataResponse(array());

        $code = $this->em
            ->getRepository('RealEstateParserBundle:User')
            ->generateCode();

        $oldAuthRecords = $this->em
            ->getRepository('RealEstate\ParserBundle\Entity\Auth')
            ->findBy(array('phone' => $phone, 'is_active' => true));

        if (count($oldAuthRecords))
        {
            foreach ($oldAuthRecords as $record)
                $record->setIsActive(false);

            $this->em->flush();
        }

        $auth = new Auth();
        $auth->setPhone($phone);
        $auth->setCode($code);
        $auth->setIsActive(true);

        $this->em->persist($auth);
        $this->em->flush();

        $smsConfig = $this->container->getParameter('sms');
        $smsSender = new SmsSender($smsConfig['api_id']);
        $sender_name = isset($smsConfig['sender_name']) ? $smsConfig['sender_name'] : null;
        $message = $smsConfig['message'] . " $code";

        $result = $smsSender->sms_send($phone, $message, $sender_name);

        return ApiController::getDataResponse(array());
    }

    public function confirmCodeAction(Request $request)
    {
        $raw_phone = ApiController::getRequestParameter('phone', $request);
        $code = ApiController::getRequestParameter('code', $request);
        $user_type_id = ApiController::getRequestParameter('user_type_id', $request);

        $delimiter = $this->container->getParameter('delimiter');
        $crypt_key = $this->container->getParameter('crypt_key');
        $testUserConfig = $this->container->getParameter('test_user');

        if (!$raw_phone || !$code)
        {
            return ApiController::getErrorResponse(array(
                'Phone number and code are required!'
            ));
        }

        $phone = trim(str_replace("+" , "", $raw_phone));

        if (!preg_match('/^' . $testUserConfig['phone_prefix'] . '/', $phone))
        {
            $auth = $this->em
                ->getRepository('RealEstate\ParserBundle\Entity\Auth')
                ->findBy(array(
                    'phone' => $phone,
                    'code' => $code,
                    'is_active' => true
                ));

            if (!$auth)
                return ApiController::getNotFoundError();
        }
        elseif ($code != $testUserConfig['code'])
            return ApiController::getNotFoundError();

        $is_user_exist = $this->em
            ->getRepository('RealEstateParserBundle:User')
            ->checkUserExistense($phone);

        if (!$is_user_exist)
        {
            $user = $this->em
                ->getRepository('RealEstateParserBundle:User')
                ->createUser($phone);

            $this->em
                ->getRepository('RealEstateParserBundle:UserDataState')
                ->initUserDataChanges($user);

            $options = array(
                'push_subscription' => true,
                'user_type_id' => $user_type_id
            );
            $this->em
                ->getRepository('RealEstateParserBundle:UserSettings')
                ->setupSettings($user, $options);
        }

        $authToken = $this->em
            ->getRepository('RealEstateParserBundle:User')
            ->authUser($phone, $delimiter, $crypt_key);

        if (!$authToken)
            return ApiController::getNotFoundError();

        $user = $this->em
            ->getRepository('RealEstateParserBundle:User')
            ->getUserByPhone($phone);

        /*$this->em
            ->getRepository('RealEstateParserBundle:Filters')
            ->deleteOldUserFilters($user);*/

        $userSettings = $user->getSettings();
        $userSettings->setPushSubscription(true);
        $this->em->flush();

        return ApiController::getDataResponse(array(
            'auth_token' => $authToken
        ));
    }

    public function savePushIdAction(Request $request)
    {
        $auth_token = ApiController::getRequestParameter('auth_token', $request);
        $uuid = ApiController::getRequestParameter('uuid', $request);

        $pushTokens = array(
            'android_reg_id' => ApiController::getRequestParameter('android_reg_id', $request),
            'apple_device_token' => ApiController::getRequestParameter('apple_device_token', $request)
        );

        $crypt_key = $this->container->getParameter('crypt_key');
        $delimiter = $this->container->getParameter('delimiter');

        $user = $this->em->getRepository('RealEstateParserBundle:User')
            ->getUserByAuthToken($auth_token, $crypt_key, $delimiter);

        if (!$user)
            return ApiController::getNotFoundError();

        $this->em
            ->getRepository('RealEstateParserBundle:MobileReg')
            ->savePushId($user, $uuid, $pushTokens);

        return ApiController::getDataResponse(array());
    }

    public function sendPushNotificationAction(Request $request)
    {
        $message = ApiController::getRequestParameter('message', $request);
        
        if (!$message)
            $message = 'База квартир обновилась.';
        
        $pushTokenRecords = $this->em
            ->getRepository('RealEstateParserBundle:MobileReg')
            ->getMobilePushTokensByUserId();
        
        $pushNotificationMessenger = new Messenger($this->container);
        $pushNotificationMessenger->setPushTokenRecords($pushTokenRecords);
        
        $value = 20;
        $message = $value . ' новых объявлений пришло';
        
        $data = array(
            'badge' => $value,
            'callback' => 'updateMyTape'
        );
        
        $pushNotificationMessenger->sendMessage($message, $data);
        
        return ApiController::getDataResponse(array());
    }

    public function savePushSubsAction(Request $request)
    {
        $push_subscription = ApiController::getRequestParameter('push_subscription', $request);
        $user = ApiController::getUser($request, $this->container);

        if (!$user->getId())
            return ApiController::getNotFoundError();

        $options = array(
            'push_subscription' => $push_subscription
        );
        $this->em
            ->getRepository('RealEstateParserBundle:UserSettings')
            ->setupSettings($user, $options);

        return ApiController::getDataResponse(array());
    }

    public function getUserDataAction(Request $request)
    {
        $user = ApiController::getUser($request, $this->container);

        if (!$user)
            return ApiController::getNotFoundError();

        $response = $this->em
            ->getRepository('RealEstateParserBundle:Tariff')
            ->getUserTariff($user->getId());

        $userData = $this->em->getRepository('RealEstateParserBundle:UserData')
            ->findOneById(UserData::SUBSCRIPTION_ID);
        $this->em->getRepository('RealEstateParserBundle:UserDataState')
            ->setChangedState($user, $userData, false);

        $userSettings = $user->getSettings();
        $response['push_notifications'] = $userSettings->getPushSubscription();

        return ApiController::getDataResponse($response);
    }

    public function initTariffPaymentAction(Request $request)
    {
        $user = ApiController::getUser($request, $this->container);
        
        if (!$user)
            return ApiController::getNotFoundError();
        
        $tariff_id = ApiController::getRequestParameter('tariff_id', $request);

        $tariff_transaction = $this->em
            ->getRepository('RealEstateParserBundle:TariffTransaction')
            ->create($user, array('tariff_id' => $tariff_id));

        if (!$tariff_transaction)
            return ApiController::getErrorResponse('Не найден тариф');

        return ApiController::getDataResponse(array(
            'tariff_id' => $tariff_transaction->getTariff()->getId(),
            'transaction_id' => $tariff_transaction->getId(),
            'sum' => $tariff_transaction->getPrice()
        )); 
    }
    
    public function checkUserDataChangesAction(Request $request)
    {
        $user = ApiController::getUser($request, $this->container);

        if (!$user)
            return ApiController::getNotFoundError();
        
        $userDataChanges = $this->em
            ->getRepository('RealEstateParserBundle:UserDataState')
            ->findBy(array('user_id' => $user->getId()));
        
        $changes = array();
        
        foreach ($userDataChanges as $userDataState)
        {
            if (!$userDataState->getIsChanged())
                continue;
            
            if ($userDataState->getUserDataId() == UserData::REALTY_ID)
                $changes['advertisements'] = true;
            
            if ($userDataState->getUserDataId() == UserData::SUBSCRIPTION_ID)
                $changes['subscription'] = true;
        }
        
        return ApiController::getDataResponse($changes);
    }
}
