<?php

namespace RealEstate\ParserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use RealEstate\ParserBundle\Controller\ApiController;
use RealEstate\ParserBundle\Entity\UserData;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FilterController extends Controller
{

    /** @DI\Inject("doctrine.orm.entity_manager") */
    protected $em;



    public function getAction(Request $request)
    {
        $user = ApiController::getUser($request, $this->container);

        if (!$user)
            return ApiController::getNotFoundError();

        $filters = $this->em
            ->getRepository('RealEstateParserBundle:Filters')
            ->getUserFilters($user);

        foreach ($filters as &$filter)
        {
            $filter["roomsCount"] = array();
            $filter["metroStations"] = array();
            $filter["metroStationRegions"] = array();
            $filter["created_at"] = $filter["created_at"]->format('Y-m-d H:i:s');

            foreach ($filter['filter_rooms_count'] as $record)
                $filter["roomsCount"][] = $record['value'];

            foreach ($filter['filter_metro_stations'] as $record)
            {
                $filter["metroStations"][] = array(
                    "id" => $record['metro_station_id'],
                    "name" => $record['name']
                );
            }

            foreach ($filter['metro_station_regions'] as $record)
                $filter["metroStationRegions"][] = $record['region_id'];

            unset($filter['user_id'], $filter['filter_rooms_count'],
                $filter['filter_metro_stations'], $filter['metro_station_regions']);
        }

        return ApiController::getDataResponse($filters);
    }

    public function saveAction(Request $request)
    {
        $user = ApiController::getUser($request, $this->container);
        $filter = ApiController::getRequestParameter('filter', $request);

        if (!$user || !$filter)
            return ApiController::getNotFoundError();

        $filter = json_decode($filter, true);

        $newFilter = $this->em
            ->getRepository('RealEstateParserBundle:Filters')
            ->saveFilter($user, $filter);

        $created_at = $newFilter->getCreatedAt()->format('Y-m-d G:i:s');

        return ApiController::getDataResponse(array(
            "id" => $newFilter->getId(),
            "is_active" => $newFilter->getIsActive(),
            "created_at" => $created_at
        ));
    }

    public function deleteAction(Request $request)
    {
        $user = ApiController::getUser($request, $this->container);
        $id = ApiController::getRequestParameter('id', $request);

        if (!$user || !$id)
            return ApiController::getNotFoundError();

        $result = $this->em
            ->getRepository('RealEstateParserBundle:Filters')
            ->deleteFilter($id);

        return ApiController::getDataResponse($result);
    }

    public function makeActiveAction(Request $request)
    {
        $user = ApiController::getUser($request, $this->container);
        $id = ApiController::getRequestParameter('id', $request);

        if (!$user || !$id)
            return ApiController::getNotFoundError();

        $this->em
            ->getRepository('RealEstateParserBundle:Filters')
            ->deactivateUserFilters($user);

        $filter = $this->em
            ->getRepository('RealEstateParserBundle:Filters')
            ->findOneById($id);

        if (!$filter)
            return ApiController::getNotFoundError();

        $filter->setIsActive(true);
        $this->em->flush();

        return ApiController::getDataResponse(true);
    }
}
