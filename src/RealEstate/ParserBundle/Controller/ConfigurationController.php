<?php

namespace RealEstate\ParserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use RealEstate\ParserBundle\Controller\ApiController;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use RealEstate\ParserBundle\Entity\User;

class ConfigurationController extends Controller
{
    /** @DI\Inject("doctrine.orm.entity_manager") */
    protected $em;

    public function getConfigAction(Request $request)
    {
        $messages = $this->em->getRepository('RealEstateParserBundle:SubscriptionMessage')->getMessages();
        $tariffs  = $this->em->getRepository('RealEstateParserBundle:Tariff')->getTariffsArray();
        $config   = array(
            'messages' => $messages,
            'tariffs'  => $tariffs
        );

        return ApiController::getDataResponse(json_encode($config));
    }

}
