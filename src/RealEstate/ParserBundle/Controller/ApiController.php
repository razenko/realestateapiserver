<?php

namespace RealEstate\ParserBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ApiController {

    private static $user = null;

    public static function getDataResponse($data) {
        return new Response(json_encode(array("data" => $data)), 200, array('content-type' => 'application/json; charset=utf-8'));
    }

    public static function getErrorResponse($error) {
        return new Response(json_encode(array("error" => $error)), 200, array('content-type' => 'application/json; charset=utf-8'));
    }

    public static function getServerErrorResponse($error) {
        return new Response(json_encode(array("error" => $error)), 500, array('content-type' => 'application/json; charset=utf-8'));
    }

    public static function getNotFoundError() {
        return new Response('Not found', 404, array());
    }

    public function errorAction(Request $request) {
        $error_message = $request->query->get('message');
        return $this::getErrorResponse(array($error_message));
    }

    public static function getRequestParameter($parameter, Request $request) {
        $post = $request->request;
        $get = $request->query;

        return $post->get($parameter) ? $post->get($parameter) : $get->get($parameter);
    }

    public static function getRequestParameters(Request $request, $container) {
        $post = $request->request;
        $get = $request->query;
        $data = array_merge($get->all(), $post->all());
        $commonFields = $container->getParameter('request_common_fields');

        foreach ($commonFields as $key)
            unset($data[$key]);

        return $data;
    }

    public static function getUser(Request $request, $container) {
        if (self::$user)
            return self::$user;
        
        $auth_token = self::getRequestParameter('auth_token', $request);

        if (!$auth_token)
            return self::getNotFoundError();

        $crypt_key = $container->getParameter('crypt_key');
        $delimiter = $container->getParameter('delimiter');

        self::$user = $container->get('doctrine.orm.entity_manager')
            ->getRepository('RealEstateParserBundle:User')
            ->getUserByAuthToken($auth_token, $crypt_key, $delimiter);
        
        return self::$user;
    }

}
