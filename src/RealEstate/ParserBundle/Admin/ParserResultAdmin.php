<?php

namespace RealEstate\ParserBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ParserResultAdmin extends Admin
{
    protected $translationDomain = 'ParserBundle';
    
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, array('label' => 'Идентификатор'))
            ->add('total_count', null, array('label' => 'Всего объявлений'))
            ->add('parsed_count', null, array('label' => 'Обработано объявлений'))
            ->add('added_count', null, array('label' => 'Добавлено объявлений'))
            ->add('updated_count', null, array('label' => 'Обновлено объявлений'))
            ->add('parsed_at', null, array('label' => 'Дата парсинга'));
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('parsed_at', null, array('label' => 'Дата парсинга'))
            ->add('added_count', null, array('label' => 'Добавлено объявлений'))
            ->add('updated_count', null, array('label' => 'Обновлено объявлений'));
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, array('label' => 'Идентификатор'))
            ->add('total_count', null, array('label' => 'Всего объявлений'))
            ->add('parsed_count', null, array('label' => 'Обработано объявлений'))
            ->add('added_count', null, array('label' => 'Добавлено объявлений'))
            ->add('updated_count', null, array('label' => 'Обновлено объявлений'))
            ->add('parsed_at', null, array('label' => 'Дата парсинга'));
    }
}