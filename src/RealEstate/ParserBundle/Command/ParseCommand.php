<?php 

namespace RealEstate\ParserBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use RealEstate\ParserBundle\Entity\Realty;
use RealEstate\ParserBundle\Mapper\RealtyMapper as Mapper;
use JMS\DiExtraBundle\Annotation as DI;
use RealEstate\ParserBundle\PushNotification\GCM;
use Goutte\Client;


class ParseCommand extends ContainerAwareCommand
{
    protected $em;
    
    
    
    public function __construct()
    {
        parent::__construct();
    }
    
    protected function configure()
    {
        $this->setName('parse')
            ->setDescription('Parse http://www.cian.ru')
            ->addArgument(
                'region_keyword',
                InputArgument::REQUIRED,
                'Keyword from DB for cian.ru region'
            )
            ->addArgument(
                'to_time',
                InputArgument::REQUIRED,
                'To time option from cian.ru'
            )
            ->addOption(
               'bench_mode',
                null,
                InputOption::VALUE_NONE,
                'Bench mode, to simulate push notifications distributions'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $region_keyword = $input->getArgument('region_keyword');
        
        $region = $em->getRepository('RealEstate\ParserBundle\Entity\Region')
            ->findOneByKeyword($region_keyword);
        
        if (!$region)
        {
            $output->writeln("Region doesn't exist in DB");
            exit;
        }
        
        $parserConfig = $this->getContainer()->getParameter('parserConfig');
        $parserConfig['bench_mode'] = (bool)$input->getOption('bench_mode');
        
        $requestParams = array(
            'obl_id' => $region->getOblId(),
            'city[0]' => $region->getCityId(),
            'totime' => $input->getArgument('to_time')
        );
        
        $mapper = $this->getContainer()->get('real_estate.parser.mapper');
        $mapper->setContainer($this->getContainer());
        $mapper->setConfig($parserConfig);
        
        $url_to_parse = $parserConfig['host'];
        $url_to_parse .= '/' . $parserConfig['script'];
        $url_to_parse .= '?';
            
        $getParameters = array();
        
        foreach ($requestParams as $key => $value)
            $getParameters[] = $key . '=' . $value;
        
        $url_to_parse .= implode('&', $getParameters);
        
        $client = new Client();
        
        $is_success = false;
        $iteration = 0;
        $max_iteration = $parserConfig['iterations']['max_global'];

        do {
            $crawler = $client->request('GET', $url_to_parse);
            $is_success = $mapper->map($crawler, $region);

            if (!$is_success)
            {
                echo 'result is empty. waiting for 10 seconds...' . PHP_EOL;
                sleep(10);

                $iteration++;
            }
        } while(!$is_success && $iteration < $max_iteration);
    }
}