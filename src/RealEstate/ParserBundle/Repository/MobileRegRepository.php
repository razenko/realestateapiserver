<?php

namespace RealEstate\ParserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RealEstate\ParserBundle\Entity\MobileReg;
use Doctrine\ORM\Mapping\ClassMetadata;

/**
 * MobileRegRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MobileRegRepository extends EntityRepository
{
    public function savePushId($user, $uuid, $pushTokens)
    {
        $em = $this->getEntityManager();
        
        if (!$user->getId())
            return;
        
        $pushSubscription = null;
                
        if ($pushTokens['android_reg_id'])
        {
            $pushSubscription = $em
                ->getRepository('RealEstate\ParserBundle\Entity\MobileReg')
                ->findOneBy(array(
                    'user_id' => $user->getId(),
                    'uuid' => $uuid
                ));
        }
        else
        {
            // apple uuid постоянно меняются, поэтому нужно выбирать запись
            // с не NULL apple_device_token
            $qb = $em->createQueryBuilder()
                ->select('mr')
                ->from('RealEstateParserBundle:MobileReg', 'mr')
                ->where('mr.user_id = :user_id')
                ->andWhere('mr.apple_device_token IS NOT NULL');
            
            $qb->setParameter('user_id', $user->getId());
            
            $pushSubscription = $qb->getQuery()->getOneOrNullResult();
        }
        
        
        if (!$pushSubscription)
        {
            $mobileReg = new MobileReg();
            $mobileReg->setUserId($user->getId());
            $mobileReg->setUser($user);
            $mobileReg->setAndroidRegId($pushTokens['android_reg_id']);
            $mobileReg->setAppleDeviceToken($pushTokens['apple_device_token']);
            $mobileReg->setUuid($uuid);

            $em->persist($mobileReg);
        }
        else
        {
            $pushSubscription->setAndroidRegId($pushTokens['android_reg_id']);
            $pushSubscription->setAppleDeviceToken($pushTokens['apple_device_token']);
        }
        
        $em->flush();
    }
    
    public function getMobilePushTokensByUserId($user_id = null, $subscribed_only = false)
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('mr')
            ->from('RealEstateParserBundle:MobileReg', 'mr')
            ->leftJoin('mr.user', 'u')
            ->leftJoin('u.settings', 'us');
        
        if ($subscribed_only)
        {
            $qb->where('us.push_subscription = :is_subscribed');
            $qb->setParameter('is_subscribed', true);
        }
        
        if ($user_id)
        {
            $qb->andWhere('u.id = :user_id');
            $qb->setParameter('user_id', $user_id);
        }
        
        try
        {
            $query = $qb->getQuery();
            $query->setFetchMode("RealEstateParserBundle:MobileReg", "address", ClassMetadata::FETCH_EAGER);
            
            return $query->getResult();
        }
        catch (\Doctrine\ORM\NoResultException $e)
        {
            return null;
        }
    }
}
