<?php 
namespace RealEstate\ParserBundle\Kendo;

class QueryParser {
	private $start_index;
	private $table_alias;
	
	private $operators = array(
		"eq" => "=",
		"neq" => "!=",
		"lt" => "<",
		"lte" => "<=",
		"gt" => ">",
		"gte" => ">="	
	);
	
	private $values = array();
	
	public function __construct(array $kendo_filter, $table_alias, $start_index = 0) {
		$this->logic = $kendo_filter['logic'];
		$this->filters = $kendo_filter['filters'];
		$this->table_alias = $table_alias;
		$this->start_index = $start_index;
	}
	
	public function getWhereDQL() {
		$DQL = '';
		
		foreach($this->filters as $index => $filter) {
			$DQL .= ' ';
			
			if ($index)
				$DQL .= $this->logic . ' ';
			
			if(isset($filter['logic'])) {
				$parser = new QueryParser($filter, $this->table_alias, count($this->values));
				$DQL .= '( ' . $parser->getWhereDQL() . ' )';
				$this->values += $parser->getWhereValues(); // можно заменить на array_merge
			}
			else {
				$value_key = 'value_' . $this->getValueIndex();
				$this->values[$value_key] = $filter['value'];
				$DQL .= $this->table_alias . '.' . $filter['field'] . ' ' . $this->operators[$filter['operator']] . " :$value_key";
			}
		}
		
		return $DQL;
	}
	
	public function getWhereValues() {
		return $this->values;
	}
	
	private function getValueIndex() {
		return count($this->values) + $this->start_index;
	}
}