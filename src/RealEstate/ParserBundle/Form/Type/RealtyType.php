<?php

namespace RealEstate\ParserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RealtyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', 'text')
            ->add('contact_phone1', 'text')
            ->add('contact_phone2', 'text')
            ->add('price', 'integer')
            ->add('payment', 'text')
            ->add('comment', 'textarea')
            ->add('deal_type', 'integer')
            ->add('term', 'integer')
            ->add('rooms_count', 'integer')
            ->add('region_id', 'entity', array(
                'class' => 'RealEstate\ParserBundle\Entity\Region',
                'property' => 'name'
            ))->add('metro', 'text')
            ->add('furniture', 'checkbox')
            ->add('kitchen_furniture', 'checkbox')
            ->add('telephone', 'checkbox')
            ->add('tv', 'checkbox')
            ->add('refrigerator', 'checkbox')
            ->add('washing_machine', 'checkbox')
            ->add('balcony', 'checkbox')
            ->add('pets', 'checkbox')
            ->add('children', 'checkbox');
    }

    public function getName()
    {
        return 'realty';
    }
}
