<?php
namespace RealEstate\ParserBundle\Model\VariablesLogger;

class VariablesLogger {
	public static function varDumpToFile($path_to_file) {
		$arguments = func_get_args();
		
		$path_to_file = trim($arguments[0]);
		$variablesToDump = array_slice($arguments, 1);
		
		$dumpData = self::getVariablesDescription($variablesToDump);
		
		file_put_contents($path_to_file, $dumpData, FILE_APPEND);
	}
	
	public static function emptyFile($path_to_file) {
		file_put_contents($path_to_file, '');
	}
	
	private static function getVariablesDescription() {
		ob_start();
		call_user_func_array('var_dump', func_get_args());
		$description = ob_get_contents();
		ob_end_clean();
		
		return $description;
	}
}