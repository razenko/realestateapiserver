<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TariffService
 */
class TariffService
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $service_id;

    /**
     * @var integer
     */
    private $tariff_id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set service_id
     *
     * @param integer $serviceId
     * @return TariffService
     */
    public function setServiceId($serviceId)
    {
        $this->service_id = $serviceId;

        return $this;
    }

    /**
     * Get service_id
     *
     * @return integer 
     */
    public function getServiceId()
    {
        return $this->service_id;
    }

    /**
     * Set tariff_id
     *
     * @param integer $tariffId
     * @return TariffService
     */
    public function setTariffId($tariffId)
    {
        $this->tariff_id = $tariffId;

        return $this;
    }

    /**
     * Get tariff_id
     *
     * @return integer 
     */
    public function getTariffId()
    {
        return $this->tariff_id;
    }
    /**
     * @var \RealEstate\ParserBundle\Entity\Tariff
     */
    private $tariff;

    /**
     * @var \RealEstate\ParserBundle\Entity\SystemService
     */
    private $system_service;


    /**
     * Set tariff
     *
     * @param \RealEstate\ParserBundle\Entity\Tariff $tariff
     * @return TariffService
     */
    public function setTariff(\RealEstate\ParserBundle\Entity\Tariff $tariff = null)
    {
        $this->tariff = $tariff;

        return $this;
    }

    /**
     * Get tariff
     *
     * @return \RealEstate\ParserBundle\Entity\Tariff 
     */
    public function getTariff()
    {
        return $this->tariff;
    }

    /**
     * Set system_service
     *
     * @param \RealEstate\ParserBundle\Entity\SystemService $systemService
     * @return TariffService
     */
    public function setSystemService(\RealEstate\ParserBundle\Entity\SystemService $systemService = null)
    {
        $this->system_service = $systemService;

        return $this;
    }

    /**
     * Get system_service
     *
     * @return \RealEstate\ParserBundle\Entity\SystemService 
     */
    public function getSystemService()
    {
        return $this->system_service;
    }
}
