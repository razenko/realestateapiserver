<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tariff
 */
class Tariff
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $keyword;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var integer
     */
    private $order;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tariff
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set keyword
     *
     * @param string $keyword
     * @return Tariff
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Get keyword
     *
     * @return string 
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Tariff
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Tariff
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }
    /**
     * @var \RealEstate\ParserBundle\Entity\TariffTransaction
     */
    private $tariff_transactions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tariff_service;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tariff_subscription;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tariff_service = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tariff_subscription = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set tariff_transactions
     *
     * @param \RealEstate\ParserBundle\Entity\TariffTransaction $tariffTransactions
     * @return Tariff
     */
    public function setTariffTransactions(\RealEstate\ParserBundle\Entity\TariffTransaction $tariffTransactions = null)
    {
        $this->tariff_transactions = $tariffTransactions;

        return $this;
    }

    /**
     * Get tariff_transactions
     *
     * @return \RealEstate\ParserBundle\Entity\TariffTransaction 
     */
    public function getTariffTransactions()
    {
        return $this->tariff_transactions;
    }

    /**
     * Add tariff_service
     *
     * @param \RealEstate\ParserBundle\Entity\TariffService $tariffService
     * @return Tariff
     */
    public function addTariffService(\RealEstate\ParserBundle\Entity\TariffService $tariffService)
    {
        $this->tariff_service[] = $tariffService;

        return $this;
    }

    /**
     * Remove tariff_service
     *
     * @param \RealEstate\ParserBundle\Entity\TariffService $tariffService
     */
    public function removeTariffService(\RealEstate\ParserBundle\Entity\TariffService $tariffService)
    {
        $this->tariff_service->removeElement($tariffService);
    }

    /**
     * Get tariff_service
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTariffService()
    {
        return $this->tariff_service;
    }

    /**
     * Add tariff_subscription
     *
     * @param \RealEstate\ParserBundle\Entity\TariffSubscription $tariffSubscription
     * @return Tariff
     */
    public function addTariffSubscription(\RealEstate\ParserBundle\Entity\TariffSubscription $tariffSubscription)
    {
        $this->tariff_subscription[] = $tariffSubscription;

        return $this;
    }

    /**
     * Remove tariff_subscription
     *
     * @param \RealEstate\ParserBundle\Entity\TariffSubscription $tariffSubscription
     */
    public function removeTariffSubscription(\RealEstate\ParserBundle\Entity\TariffSubscription $tariffSubscription)
    {
        $this->tariff_subscription->removeElement($tariffSubscription);
    }

    /**
     * Get tariff_subscription
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTariffSubscription()
    {
        return $this->tariff_subscription;
    }
    /**
     * @var integer
     */
    private $days_of_subscription;


    /**
     * Set days_of_subscription
     *
     * @param integer $daysOfSubscription
     * @return Tariff
     */
    public function setDaysOfSubscription($daysOfSubscription)
    {
        $this->days_of_subscription = $daysOfSubscription;

        return $this;
    }

    /**
     * Get days_of_subscription
     *
     * @return integer 
     */
    public function getDaysOfSubscription()
    {
        return $this->days_of_subscription;
    }
    /**
     * @var integer
     */
    private $days;


    /**
     * Set days
     *
     * @param integer $days
     * @return Tariff
     */
    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * Get days
     *
     * @return integer 
     */
    public function getDays()
    {
        return $this->days;
    }
    /**
     * @var string
     */
    private $currency_key;


    /**
     * Set currency_key
     *
     * @param string $currencyKey
     * @return Tariff
     */
    public function setCurrencyKey($currencyKey)
    {
        $this->currency_key = $currencyKey;

        return $this;
    }

    /**
     * Get currency_key
     *
     * @return string 
     */
    public function getCurrencyKey()
    {
        return $this->currency_key;
    }
}
