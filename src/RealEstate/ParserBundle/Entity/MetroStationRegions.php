<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MetroStationRegions
 */
class MetroStationRegions
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var integer
     */
    private $region_id;

    /**
     * @var \RealEstate\ParserBundle\Entity\Filters
     */
    private $filters;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return MetroStationRegions
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set region_id
     *
     * @param integer $regionId
     * @return MetroStationRegions
     */
    public function setRegionId($regionId)
    {
        $this->region_id = $regionId;

        return $this;
    }

    /**
     * Get region_id
     *
     * @return integer 
     */
    public function getRegionId()
    {
        return $this->region_id;
    }

    /**
     * Set filters
     *
     * @param \RealEstate\ParserBundle\Entity\Filters $filters
     * @return MetroStationRegions
     */
    public function setFilters(\RealEstate\ParserBundle\Entity\Filters $filters = null)
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * Get filters
     *
     * @return \RealEstate\ParserBundle\Entity\Filters 
     */
    public function getFilters()
    {
        return $this->filters;
    }
    /**
     * @var \RealEstate\ParserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \RealEstate\ParserBundle\Entity\User $user
     * @return MetroStationRegions
     */
    public function setUser(\RealEstate\ParserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RealEstate\ParserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
