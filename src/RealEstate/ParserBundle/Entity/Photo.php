<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Photo
 */
class Photo
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $realty_id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var \RealEstate\ParserBundle\Entity\Realty
     */
    private $realty;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set realty_id
     *
     * @param integer $realtyId
     * @return Photo
     */
    public function setRealtyId($realtyId)
    {
        $this->realty_id = $realtyId;

        return $this;
    }

    /**
     * Get realty_id
     *
     * @return integer 
     */
    public function getRealtyId()
    {
        return $this->realty_id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Photo
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set realty
     *
     * @param \RealEstate\ParserBundle\Entity\Realty $realty
     * @return Photo
     */
    public function setRealty(\RealEstate\ParserBundle\Entity\Realty $realty = null)
    {
        $this->realty = $realty;

        return $this;
    }

    /**
     * Get realty
     *
     * @return \RealEstate\ParserBundle\Entity\Realty 
     */
    public function getRealty()
    {
        return $this->realty;
    }
    
    public function __toString()
    {
        return $this->getUrl();
    }
}
