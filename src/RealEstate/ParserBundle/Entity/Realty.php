<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RealEstate\ParserBundle\Entity\Photo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Realty
 */
class Realty
{
    const FLAT_DEAL_TYPE = 1;
    const HOUSE_DEAL_TYPE = 2;
    const NON_RESIDENTAL_PREMISEDEAL_TYPE = 3;

    const DAY_TERM = 1;
    const MONTH_TERM = 3;
    const LONG_TERM = 4;

    const ONE_ROOM = 0;
    const MANY_ROOMS = 6;


        /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $remote_id;

    /**
     * @var integer
     */
    private $realty_type_id;

    /**
     * @var integer
     */
    private $count_room;

    /**
     * @var integer
     */
    private $deal_type;

    /**
     * @var integer
     */
    private $region_id;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $metro;

    /**
     * @var integer
     */
    private $common_area;

    /**
     * @var integer
     */
    private $rooms_area;

    /**
     * @var integer
     */
    private $living_area;

    /**
     * @var integer
     */
    private $kitchen_area;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var integer
     */
    private $milestone;

    /**
     * @var string
     */
    private $term;

    /**
     * @var string
     */
    private $floors;

    /**
     * @var string
     */
    private $percentage;

    /**
     * @var boolean
     */
    private $furniture;

    /**
     * @var boolean
     */
    private $kitchen_furniture;

    /**
     * @var boolean
     */
    private $telephone;

    /**
     * @var boolean
     */
    private $tv;

    /**
     * @var boolean
     */
    private $refrigerator;

    /**
     * @var boolean
     */
    private $washing_machine;

    /**
     * @var boolean
     */
    private $balcony;

    /**
     * @var boolean
     */
    private $pets;

    /**
     * @var boolean
     */
    private $children;

    /**
     * @var string
     */
    private $contact_phone1;

    /**
     * @var string
     */
    private $contact_phone2;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $hash;

    public function __construct()
    {
        $this->deal_type = 1;
        $this->photos = new ArrayCollection();
    }

    public static function getDealTypes()
    {
        return array(self::FLAT_DEAL_TYPE, self::HOUSE_DEAL_TYPE, self::NON_RESIDENTAL_PREMISEDEAL_TYPE);
    }

    public static function getTerms()
    {
        return array(self::DAY_TERM, self::MONTH_TERM, self::LONG_TERM);
    }
    
    public function makeTitle()
    {
        $roomTitleParts = array(
            self::ONE_ROOM => 'комната',
            1 => '1-комн. кв.',
            2 => '2-комн. кв.',
            3 => '3-комн. кв.',
            4 => '4-комн. кв.',
            5 => '5-комн. кв.',
            self::MANY_ROOMS => 'многокомн. кв.'
        );
        $termTitleParts = array(
            self::DAY_TERM => 'посуточно',
            self::MONTH_TERM => 'на несколько месяцев (меньше года)',
            self::LONG_TERM => 'на длительный срок (от года)'
        );

        $roomTitle = $roomTitleParts[$this->getRoomsCount()];
        $termTitle = $termTitleParts[$this->getTerm()];

        return "{$roomTitle} {$termTitle}";
    }

    public function makeHash()
    {
        $data = '';
        $data .= $this->getRemoteId();
        $data .= $this->getPercentage();
        $data .= $this->getFloors();
        $data .= $this->getTitle();
        $data .= $this->getFurniture();
        $data .= $this->getKitchenFurniture();
        $data .= $this->getTelephone();
        $data .= $this->getTv();
        $data .= $this->getWashingMachine();
        $data .= $this->getRefrigerator();
        $data .= $this->getBalcony();
        $data .= $this->getPets();
        $data .= $this->getChildren();
        $data .= $this->getContactPhone1();
        $data .= $this->getAddress();
        $data .= $this->getLongitude();
        $data .= $this->getLatitude();
        $data .= $this->getMetro();
        $data .= $this->getPrice();
        $data .= $this->getMilestone();
        $data .= $this->getPayment();
        $data .= $this->getTerm();
        $data .= $this->getCommonArea();
        $data .= $this->getRoomsArea();
        $data .= $this->getLivingArea();
        $data .= $this->getKitchenArea();
        $data .= $this->getComment();

        /*if ($this['photos'])
        {
            foreach ($this['photos'] as $photo)
                $data .= $photo;
        }*/

        return sha1($data);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remote_id
     *
     * @param integer $remoteId
     * @return Realty
     */
    public function setRemoteId($remoteId)
    {
        $this->remote_id = $remoteId;

        return $this;
    }

    /**
     * Get remote_id
     *
     * @return integer 
     */
    public function getRemoteId()
    {
        return $this->remote_id;
    }

    /**
     * Set realty_type_id
     *
     * @param integer $realtyTypeId
     * @return Realty
     */
    public function setRealtyTypeId($realtyTypeId)
    {
        $this->realty_type_id = $realtyTypeId;

        return $this;
    }

    /**
     * Get realty_type_id
     *
     * @return integer 
     */
    public function getRealtyTypeId()
    {
        return $this->realty_type_id;
    }

    /**
     * Set count_room
     *
     * @param integer $countRoom
     * @return Realty
     */
    public function setCountRoom($countRoom)
    {
        $this->count_room = $countRoom;

        return $this;
    }

    /**
     * Get count_room
     *
     * @return integer 
     */
    public function getCountRoom()
    {
        return $this->count_room;
    }

    /**
     * Set deal_type
     *
     * @param integer $dealType
     * @return Realty
     */
    public function setDealType($dealType)
    {
        $this->deal_type = $dealType;

        return $this;
    }

    /**
     * Get deal_type
     *
     * @return integer 
     */
    public function getDealType()
    {
        return $this->deal_type;
    }

    /**
     * Set region_id
     *
     * @param integer $regionId
     * @return Realty
     */
    public function setRegionId($regionId)
    {
        $this->region_id = $regionId;

        return $this;
    }

    /**
     * Get region_id
     *
     * @return integer 
     */
    public function getRegionId()
    {
        return $this->region_id;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Realty
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set metro
     *
     * @param string $metro
     * @return Realty
     */
    public function setMetro($metro)
    {
        $this->metro = $metro;

        return $this;
    }

    /**
     * Get metro
     *
     * @return string 
     */
    public function getMetro()
    {
        return $this->metro;
    }

    /**
     * Set common_area
     *
     * @param integer $commonArea
     * @return Realty
     */
    public function setCommonArea($commonArea)
    {
        $this->common_area = $commonArea;

        return $this;
    }

    /**
     * Get common_area
     *
     * @return integer 
     */
    public function getCommonArea()
    {
        return $this->common_area;
    }

    /**
     * Set rooms_area
     *
     * @param integer $roomsArea
     * @return Realty
     */
    public function setRoomsArea($roomsArea)
    {
        $this->rooms_area = $roomsArea;

        return $this;
    }

    /**
     * Get rooms_area
     *
     * @return integer 
     */
    public function getRoomsArea()
    {
        return $this->rooms_area;
    }

    /**
     * Set living_area
     *
     * @param integer $livingArea
     * @return Realty
     */
    public function setLivingArea($livingArea)
    {
        $this->living_area = $livingArea;

        return $this;
    }

    /**
     * Get living_area
     *
     * @return integer 
     */
    public function getLivingArea()
    {
        return $this->living_area;
    }

    /**
     * Set kitchen_area
     *
     * @param integer $kitchenArea
     * @return Realty
     */
    public function setKitchenArea($kitchenArea)
    {
        $this->kitchen_area = $kitchenArea;

        return $this;
    }

    /**
     * Get kitchen_area
     *
     * @return integer 
     */
    public function getKitchenArea()
    {
        return $this->kitchen_area;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Realty
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set milestone
     *
     * @param integer $milestone
     * @return Realty
     */
    public function setMilestone($milestone)
    {
        $this->milestone = $milestone;

        return $this;
    }

    /**
     * Get milestone
     *
     * @return integer 
     */
    public function getMilestone()
    {
        return $this->milestone;
    }

    /**
     * Set term
     *
     * @param string $term
     * @return Realty
     */
    public function setTerm($term)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return string 
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set floors
     *
     * @param string $floors
     * @return Realty
     */
    public function setFloors($floors)
    {
        $this->floors = $floors;

        return $this;
    }

    /**
     * Get floors
     *
     * @return string 
     */
    public function getFloors()
    {
        return $this->floors;
    }

    /**
     * Set percentage
     *
     * @param string $percentage
     * @return Realty
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return string 
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set furniture
     *
     * @param boolean $furniture
     * @return Realty
     */
    public function setFurniture($furniture)
    {
        $this->furniture = $furniture;

        return $this;
    }

    /**
     * Get furniture
     *
     * @return boolean 
     */
    public function getFurniture()
    {
        return $this->furniture;
    }

    /**
     * Set kitchen_furniture
     *
     * @param boolean $kitchenFurniture
     * @return Realty
     */
    public function setKitchenFurniture($kitchenFurniture)
    {
        $this->kitchen_furniture = $kitchenFurniture;

        return $this;
    }

    /**
     * Get kitchen_furniture
     *
     * @return boolean 
     */
    public function getKitchenFurniture()
    {
        return $this->kitchen_furniture;
    }

    /**
     * Set telephone
     *
     * @param boolean $telephone
     * @return Realty
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return boolean 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set tv
     *
     * @param boolean $tv
     * @return Realty
     */
    public function setTv($tv)
    {
        $this->tv = $tv;

        return $this;
    }

    /**
     * Get tv
     *
     * @return boolean 
     */
    public function getTv()
    {
        return $this->tv;
    }

    /**
     * Set refrigerator
     *
     * @param boolean $refrigerator
     * @return Realty
     */
    public function setRefrigerator($refrigerator)
    {
        $this->refrigerator = $refrigerator;

        return $this;
    }

    /**
     * Get refrigerator
     *
     * @return boolean 
     */
    public function getRefrigerator()
    {
        return $this->refrigerator;
    }

    /**
     * Set washing_machine
     *
     * @param boolean $washingMachine
     * @return Realty
     */
    public function setWashingMachine($washingMachine)
    {
        $this->washing_machine = $washingMachine;

        return $this;
    }

    /**
     * Get washing_machine
     *
     * @return boolean 
     */
    public function getWashingMachine()
    {
        return $this->washing_machine;
    }

    /**
     * Set balcony
     *
     * @param boolean $balcony
     * @return Realty
     */
    public function setBalcony($balcony)
    {
        $this->balcony = $balcony;

        return $this;
    }

    /**
     * Get balcony
     *
     * @return boolean 
     */
    public function getBalcony()
    {
        return $this->balcony;
    }

    /**
     * Set pets
     *
     * @param boolean $pets
     * @return Realty
     */
    public function setPets($pets)
    {
        $this->pets = $pets;

        return $this;
    }

    /**
     * Get pets
     *
     * @return boolean 
     */
    public function getPets()
    {
        return $this->pets;
    }

    /**
     * Set children
     *
     * @param boolean $children
     * @return Realty
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return boolean 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set contact_phone1
     *
     * @param string $contactPhone1
     * @return Realty
     */
    public function setContactPhone1($contactPhone1)
    {
        $this->contact_phone1 = $contactPhone1;

        return $this;
    }

    /**
     * Get contact_phone1
     *
     * @return string 
     */
    public function getContactPhone1()
    {
        return $this->contact_phone1;
    }

    /**
     * Set contact_phone2
     *
     * @param string $contactPhone2
     * @return Realty
     */
    public function setContactPhone2($contactPhone2)
    {
        $this->contact_phone2 = $contactPhone2;

        return $this;
    }

    /**
     * Get contact_phone2
     *
     * @return string 
     */
    public function getContactPhone2()
    {
        return $this->contact_phone2;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Realty
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return Realty
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $photos;


    /**
     * Add photos
     *
     * @param \RealEstate\ParserBundle\Entity\Photos $photos
     * @return Realty
     */
    
    public function addPhoto(\RealEstate\ParserBundle\Entity\Photo $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \RealEstate\ParserBundle\Entity\Photos $photos
     */
    public function removePhoto(\RealEstate\ParserBundle\Entity\Photo $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhotos()
    {
        return $this->photos;
    }
    /**
     * @var integer
     */
    private $rooms_count;


    /**
     * Set rooms_count
     *
     * @param integer $roomsCount
     * @return Realty
     */
    public function setRoomsCount($roomsCount)
    {
        $this->rooms_count = $roomsCount;

        return $this;
    }

    /**
     * Get rooms_count
     *
     * @return integer 
     */
    public function getRoomsCount()
    {
        return $this->rooms_count;
    }
    /**
     * @var \DateTime
     */
    private $created_at_date;

    /**
     * @var \DateTime
     */
    private $created_at_time;


    /**
     * Set created_at_date
     *
     * @param \DateTime $createdAtDate
     * @return Realty
     */
    public function setCreatedAtDate($createdAtDate)
    {
        $this->created_at_date = $createdAtDate;

        return $this;
    }

    /**
     * Get created_at_date
     *
     * @return \DateTime 
     */
    public function getCreatedAtDate()
    {
        return $this->created_at_date;
    }

    /**
     * Set created_at_time
     *
     * @param \DateTime $createdAtTime
     * @return Realty
     */
    public function setCreatedAtTime($createdAtTime)
    {
        $this->created_at_time = $createdAtTime;

        return $this;
    }

    /**
     * Get created_at_time
     *
     * @return \DateTime 
     */
    public function getCreatedAtTime()
    {
        return $this->created_at_time;
    }
    /**
     * @var \DateTime
     */
    private $updated_at_date;

    /**
     * @var \DateTime
     */
    private $updated_at_time;


    /**
     * Set updated_at_date
     *
     * @param \DateTime $updatedAtDate
     * @return Realty
     */
    public function setUpdatedAtDate($updatedAtDate)
    {
        $this->updated_at_date = $updatedAtDate;

        return $this;
    }

    /**
     * Get updated_at_date
     *
     * @return \DateTime 
     */
    public function getUpdatedAtDate()
    {
        return $this->updated_at_date;
    }

    /**
     * Set updated_at_time
     *
     * @param \DateTime $updatedAtTime
     * @return Realty
     */
    public function setUpdatedAtTime($updatedAtTime)
    {
        $this->updated_at_time = $updatedAtTime;

        return $this;
    }

    /**
     * Get updated_at_time
     *
     * @return \DateTime 
     */
    public function getUpdatedAtTime()
    {
        return $this->updated_at_time;
    }
    /**
     * @var string
     */
    private $map_points;


    /**
     * Set map_points
     *
     * @param string $mapPoints
     * @return Realty
     */
    public function setMapPoints($mapPoints)
    {
        $this->map_points = $mapPoints;

        return $this;
    }

    /**
     * Get map_points
     *
     * @return string 
     */
    public function getMapPoints()
    {
        return $this->map_points;
    }
    /**
     * @var string
     */
    private $latitude;

    /**
     * @var string
     */
    private $longitude;


    /**
     * Set latitude
     *
     * @param string $latitude
     * @return Realty
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return Realty
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
    /**
     * @var string
     */
    private $payment;


    /**
     * Set payment
     *
     * @param string $payment
     * @return Realty
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return string 
     */
    public function getPayment()
    {
        return $this->payment;
    }
    /**
     * @var string
     */
    private $title;


    /**
     * Set title
     *
     * @param string $title
     * @return Realty
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
    /**
     * @var \RealEstate\ParserBundle\Entity\Region
     */
    private $region;


    /**
     * Set region
     *
     * @param \RealEstate\ParserBundle\Entity\Region $region
     * @return Realty
     */
    public function setRegion(\RealEstate\ParserBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \RealEstate\ParserBundle\Entity\Region 
     */
    public function getRegion()
    {
        return $this->region;
    }
}
