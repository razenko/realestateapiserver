<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SystemService
 */
class SystemService
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $keyword;

    /**
     * @var boolean
     */
    private $is_active;

    /**
     * @var string
     */
    private $note_text;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tariff_service;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tariff_service = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SystemService
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set keyword
     *
     * @param string $keyword
     * @return SystemService
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Get keyword
     *
     * @return string 
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return SystemService
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set note_text
     *
     * @param string $noteText
     * @return SystemService
     */
    public function setNoteText($noteText)
    {
        $this->note_text = $noteText;

        return $this;
    }

    /**
     * Get note_text
     *
     * @return string 
     */
    public function getNoteText()
    {
        return $this->note_text;
    }

    /**
     * Add tariff_service
     *
     * @param \RealEstate\ParserBundle\Entity\TariffService $tariffService
     * @return SystemService
     */
    public function addTariffService(\RealEstate\ParserBundle\Entity\TariffService $tariffService)
    {
        $this->tariff_service[] = $tariffService;

        return $this;
    }

    /**
     * Remove tariff_service
     *
     * @param \RealEstate\ParserBundle\Entity\TariffService $tariffService
     */
    public function removeTariffService(\RealEstate\ParserBundle\Entity\TariffService $tariffService)
    {
        $this->tariff_service->removeElement($tariffService);
    }

    /**
     * Get tariff_service
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTariffService()
    {
        return $this->tariff_service;
    }
}
