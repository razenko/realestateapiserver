<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TariffSubscription
 */
class TariffSubscription
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var integer
     */
    private $tariff_id;

    /**
     * @var \DateTime
     */
    private $subscribed_till;

    /**
     * @var float
     */
    private $price;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return TariffSubscription
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set tariff_id
     *
     * @param integer $tariffId
     * @return TariffSubscription
     */
    public function setTariffId($tariffId)
    {
        $this->tariff_id = $tariffId;

        return $this;
    }

    /**
     * Get tariff_id
     *
     * @return integer 
     */
    public function getTariffId()
    {
        return $this->tariff_id;
    }

    /**
     * Set subscribed_till
     *
     * @param \DateTime $subscribedTill
     * @return TariffSubscription
     */
    public function setSubscribedTill($subscribedTill)
    {
        $this->subscribed_till = $subscribedTill;

        return $this;
    }

    /**
     * Get subscribed_till
     *
     * @return \DateTime 
     */
    public function getSubscribedTill()
    {
        return $this->subscribed_till;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return TariffSubscription
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * @var \RealEstate\ParserBundle\Entity\User
     */
    private $user;

    /**
     * @var \RealEstate\ParserBundle\Entity\TariffTransaction
     */
    private $tariff_transactions;

    /**
     * @var \RealEstate\ParserBundle\Entity\Tariff
     */
    private $tariff;


    /**
     * Set user
     *
     * @param \RealEstate\ParserBundle\Entity\User $user
     * @return TariffSubscription
     */
    public function setUser(\RealEstate\ParserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RealEstate\ParserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set tariff_transactions
     *
     * @param \RealEstate\ParserBundle\Entity\TariffTransaction $tariffTransactions
     * @return TariffSubscription
     */
    public function setTariffTransactions(\RealEstate\ParserBundle\Entity\TariffTransaction $tariffTransactions = null)
    {
        $this->tariff_transactions = $tariffTransactions;

        return $this;
    }

    /**
     * Get tariff_transactions
     *
     * @return \RealEstate\ParserBundle\Entity\TariffTransaction 
     */
    public function getTariffTransactions()
    {
        return $this->tariff_transactions;
    }

    /**
     * Set tariff
     *
     * @param \RealEstate\ParserBundle\Entity\Tariff $tariff
     * @return TariffSubscription
     */
    public function setTariff(\RealEstate\ParserBundle\Entity\Tariff $tariff = null)
    {
        $this->tariff = $tariff;

        return $this;
    }

    /**
     * Get tariff
     *
     * @return \RealEstate\ParserBundle\Entity\Tariff 
     */
    public function getTariff()
    {
        return $this->tariff;
    }
    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updated_at;


    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return TariffSubscription
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return TariffSubscription
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
}
