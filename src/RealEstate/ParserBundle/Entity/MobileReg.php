<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MobileReg
 */
class MobileReg
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var string
     */
    private $android_reg_id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return MobileReg
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set android_reg_id
     *
     * @param string $androidRegId
     * @return MobileReg
     */
    public function setAndroidRegId($androidRegId)
    {
        $this->android_reg_id = $androidRegId;

        return $this;
    }

    /**
     * Get android_reg_id
     *
     * @return string 
     */
    public function getAndroidRegId()
    {
        return $this->android_reg_id;
    }
    /**
     * @var string
     */
    private $uuid;


    /**
     * Set uuid
     *
     * @param string $uuid
     * @return MobileReg
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string 
     */
    public function getUuid()
    {
        return $this->uuid;
    }
    /**
     * @var \RealEstate\ParserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \RealEstate\ParserBundle\Entity\User $user
     * @return MobileReg
     */
    public function setUser(\RealEstate\ParserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RealEstate\ParserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var string
     */
    private $apple_device_token;


    /**
     * Set apple_device_token
     *
     * @param string $appleDeviceToken
     * @return MobileReg
     */
    public function setAppleDeviceToken($appleDeviceToken)
    {
        $this->apple_device_token = $appleDeviceToken;

        return $this;
    }

    /**
     * Get apple_device_token
     *
     * @return string 
     */
    public function getAppleDeviceToken()
    {
        return $this->apple_device_token;
    }
}
