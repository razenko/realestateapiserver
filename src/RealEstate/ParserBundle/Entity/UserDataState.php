<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserDataState
 */
class UserDataState
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var integer
     */
    private $data_id;

    /**
     * @var boolean
     */
    private $is_changed;

    /**
     * @var \RealEstate\ParserBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return UserDataState
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set data_id
     *
     * @param integer $dataId
     * @return UserDataState
     */
    public function setDataId($dataId)
    {
        $this->data_id = $dataId;

        return $this;
    }

    /**
     * Get data_id
     *
     * @return integer 
     */
    public function getDataId()
    {
        return $this->data_id;
    }

    /**
     * Set is_changed
     *
     * @param boolean $isChanged
     * @return UserDataState
     */
    public function setIsChanged($isChanged)
    {
        $this->is_changed = $isChanged;

        return $this;
    }

    /**
     * Get is_changed
     *
     * @return boolean 
     */
    public function getIsChanged()
    {
        return $this->is_changed;
    }

    /**
     * Set user
     *
     * @param \RealEstate\ParserBundle\Entity\User $user
     * @return UserDataState
     */
    public function setUser(\RealEstate\ParserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RealEstate\ParserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var \RealEstate\ParserBundle\Entity\UserData
     */
    private $user_data;


    /**
     * Set user_data
     *
     * @param \RealEstate\ParserBundle\Entity\UserData $userData
     * @return UserDataState
     */
    public function setUserData(\RealEstate\ParserBundle\Entity\UserData $userData = null)
    {
        $this->user_data = $userData;

        return $this;
    }

    /**
     * Get user_data
     *
     * @return \RealEstate\ParserBundle\Entity\UserData 
     */
    public function getUserData()
    {
        return $this->user_data;
    }
    /**
     * @var integer
     */
    private $user_data_id;


    /**
     * Set user_data_id
     *
     * @param integer $userDataId
     * @return UserDataState
     */
    public function setUserDataId($userDataId)
    {
        $this->user_data_id = $userDataId;

        return $this;
    }

    /**
     * Get user_data_id
     *
     * @return integer 
     */
    public function getUserDataId()
    {
        return $this->user_data_id;
    }
}
