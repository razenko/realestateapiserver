<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubscriptionMessage
 */
class SubscriptionMessage
{
	static $statuses = array(
			1 => 'first',
			2 => 'trial',
			3 => 'active',
	);
	
	static $status_types = array(
			1 => 'trial',
			2 => 'finished1',
			3 => 'finished2',
	);
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $text;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return SubscriptionMessage
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }
    /**
     * @var integer
     */
    private $state;

    /**
     * @var integer
     */
    private $state_type;


    /**
     * Set state
     *
     * @param integer $state
     * @return SubscriptionMessage
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set state_type
     *
     * @param integer $stateType
     * @return SubscriptionMessage
     */
    public function setStateType($stateType)
    {
        $this->state_type = $stateType;

        return $this;
    }

    /**
     * Get state_type
     *
     * @return integer 
     */
    public function getStateType()
    {
        return $this->state_type;
    }
}
