<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParserResult
 */
class ParserResult
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $total_count;

    /**
     * @var integer
     */
    private $parsed_count;

    /**
     * @var integer
     */
    private $added_count;

    /**
     * @var \DateTime
     */
    private $parsed_at;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set total_count
     *
     * @param integer $totalCount
     * @return ParserResult
     */
    public function setTotalCount($totalCount)
    {
        $this->total_count = $totalCount;

        return $this;
    }

    /**
     * Get total_count
     *
     * @return integer 
     */
    public function getTotalCount()
    {
        return $this->total_count;
    }

    /**
     * Set parsed_count
     *
     * @param integer $parsedCount
     * @return ParserResult
     */
    public function setParsedCount($parsedCount)
    {
        $this->parsed_count = $parsedCount;

        return $this;
    }

    /**
     * Get parsed_count
     *
     * @return integer 
     */
    public function getParsedCount()
    {
        return $this->parsed_count;
    }

    /**
     * Set added_count
     *
     * @param integer $addedCount
     * @return ParserResult
     */
    public function setAddedCount($addedCount)
    {
        $this->added_count = $addedCount;

        return $this;
    }

    /**
     * Get added_count
     *
     * @return integer 
     */
    public function getAddedCount()
    {
        return $this->added_count;
    }

    /**
     * Set parsed_at
     *
     * @param \DateTime $parsedAt
     * @return ParserResult
     */
    public function setParsedAt($parsedAt)
    {
        $this->parsed_at = $parsedAt;

        return $this;
    }

    /**
     * Get parsed_at
     *
     * @return \DateTime 
     */
    public function getParsedAt()
    {
        return $this->parsed_at;
    }
    /**
     * @var integer
     */
    private $updated_count;


    /**
     * Set updated_count
     *
     * @param integer $updatedCount
     * @return ParserResult
     */
    public function setUpdatedCount($updatedCount)
    {
        $this->updated_count = $updatedCount;

        return $this;
    }

    /**
     * Get updated_count
     *
     * @return integer 
     */
    public function getUpdatedCount()
    {
        return $this->updated_count;
    }
}
