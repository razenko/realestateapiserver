<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FilterMetroStations
 */
class FilterMetroStations
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var integer
     */
    private $metro_station_id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \RealEstate\ParserBundle\Entity\Filters
     */
    private $filters;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return FilterMetroStations
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set metro_station_id
     *
     * @param integer $metroStationId
     * @return FilterMetroStations
     */
    public function setMetroStationId($metroStationId)
    {
        $this->metro_station_id = $metroStationId;

        return $this;
    }

    /**
     * Get metro_station_id
     *
     * @return integer 
     */
    public function getMetroStationId()
    {
        return $this->metro_station_id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return FilterMetroStations
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set filters
     *
     * @param \RealEstate\ParserBundle\Entity\Filters $filters
     * @return FilterMetroStations
     */
    public function setFilters(\RealEstate\ParserBundle\Entity\Filters $filters = null)
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * Get filters
     *
     * @return \RealEstate\ParserBundle\Entity\Filters 
     */
    public function getFilters()
    {
        return $this->filters;
    }
    /**
     * @var integer
     */
    private $filter_id;


    /**
     * Set filter_id
     *
     * @param integer $filterId
     * @return FilterMetroStations
     */
    public function setFilterId($filterId)
    {
        $this->filter_id = $filterId;

        return $this;
    }

    /**
     * Get filter_id
     *
     * @return integer 
     */
    public function getFilterId()
    {
        return $this->filter_id;
    }
    /**
     * @var \RealEstate\ParserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \RealEstate\ParserBundle\Entity\User $user
     * @return FilterMetroStations
     */
    public function setUser(\RealEstate\ParserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RealEstate\ParserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
