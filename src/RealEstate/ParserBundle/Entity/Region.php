<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Region
 */
class Region
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Region
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @var string
     */
    private $keyword;

    /**
     * @var integer
     */
    private $obl_id;

    /**
     * @var integer
     */
    private $city_id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $realties;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->realties = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set keyword
     *
     * @param string $keyword
     * @return Region
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Get keyword
     *
     * @return string 
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * Set obl_id
     *
     * @param integer $oblId
     * @return Region
     */
    public function setOblId($oblId)
    {
        $this->obl_id = $oblId;

        return $this;
    }

    /**
     * Get obl_id
     *
     * @return integer 
     */
    public function getOblId()
    {
        return $this->obl_id;
    }

    /**
     * Set city_id
     *
     * @param integer $cityId
     * @return Region
     */
    public function setCityId($cityId)
    {
        $this->city_id = $cityId;

        return $this;
    }

    /**
     * Get city_id
     *
     * @return integer 
     */
    public function getCityId()
    {
        return $this->city_id;
    }

    /**
     * Add realties
     *
     * @param \RealEstate\ParserBundle\Entity\Realty $realties
     * @return Region
     */
    public function addRealty(\RealEstate\ParserBundle\Entity\Realty $realties)
    {
        $this->realties[] = $realties;

        return $this;
    }

    /**
     * Remove realties
     *
     * @param \RealEstate\ParserBundle\Entity\Realty $realties
     */
    public function removeRealty(\RealEstate\ParserBundle\Entity\Realty $realties)
    {
        $this->realties->removeElement($realties);
    }

    /**
     * Get realties
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRealties()
    {
        return $this->realties;
    }
    /**
     * @var boolean
     */
    private $has_metro;


    /**
     * Set has_metro
     *
     * @param boolean $hasMetro
     * @return Region
     */
    public function setHasMetro($hasMetro)
    {
        $this->has_metro = $hasMetro;

        return $this;
    }

    /**
     * Get has_metro
     *
     * @return boolean 
     */
    public function getHasMetro()
    {
        return $this->has_metro;
    }
}
