<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserRealty
 */
class UserRealty
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var integer
     */
    private $realty_id;

    /**
     * @var boolean
     */
    private $is_favorite;

    /**
     * @var \RealEstate\ParserBundle\Entity\Realty
     */
    private $realty;

    /**
     * @var \RealEstate\ParserBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return UserRealty
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set realty_id
     *
     * @param integer $realtyId
     * @return UserRealty
     */
    public function setRealtyId($realtyId)
    {
        $this->realty_id = $realtyId;

        return $this;
    }

    /**
     * Get realty_id
     *
     * @return integer 
     */
    public function getRealtyId()
    {
        return $this->realty_id;
    }

    /**
     * Set is_favorite
     *
     * @param boolean $isFavorite
     * @return UserRealty
     */
    public function setIsFavorite($isFavorite)
    {
        $this->is_favorite = $isFavorite;

        return $this;
    }

    /**
     * Get is_favorite
     *
     * @return boolean 
     */
    public function getIsFavorite()
    {
        return $this->is_favorite;
    }

    /**
     * Set realty
     *
     * @param \RealEstate\ParserBundle\Entity\Realty $realty
     * @return UserRealty
     */
    public function setRealty(\RealEstate\ParserBundle\Entity\Realty $realty = null)
    {
        $this->realty = $realty;

        return $this;
    }

    /**
     * Get realty
     *
     * @return \RealEstate\ParserBundle\Entity\Realty 
     */
    public function getRealty()
    {
        return $this->realty;
    }

    /**
     * Set user
     *
     * @param \RealEstate\ParserBundle\Entity\User $user
     * @return UserRealty
     */
    public function setUser(\RealEstate\ParserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RealEstate\ParserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
