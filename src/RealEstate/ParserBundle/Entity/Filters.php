<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Filters
 */
class Filters
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var integer
     */
    private $deal_type;

    /**
     * @var integer
     */
    private $min_price;

    /**
     * @var integer
     */
    private $max_price;

    /**
     * @var integer
     */
    private $term;

    /**
     * @var integer
     */
    private $region_id;

    /**
     * @var boolean
     */
    private $furniture;

    /**
     * @var boolean
     */
    private $kitchen_furniture;

    /**
     * @var boolean
     */
    private $telephone;

    /**
     * @var boolean
     */
    private $tv;

    /**
     * @var boolean
     */
    private $refrigerator;

    /**
     * @var boolean
     */
    private $washing_machine;

    /**
     * @var boolean
     */
    private $balcony;

    /**
     * @var boolean
     */
    private $pets;

    /**
     * @var boolean
     */
    private $children;

    /**
     * @var integer
     */
    private $filter_rooms_count_id;

    /**
     * @var integer
     */
    private $filter_metro_stations_id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return Filters
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set deal_type
     *
     * @param integer $dealType
     * @return Filters
     */
    public function setDealType($dealType)
    {
        $this->deal_type = $dealType;

        return $this;
    }

    /**
     * Get deal_type
     *
     * @return integer 
     */
    public function getDealType()
    {
        return $this->deal_type;
    }

    /**
     * Set min_price
     *
     * @param integer $minPrice
     * @return Filters
     */
    public function setMinPrice($minPrice)
    {
        $this->min_price = $minPrice;

        return $this;
    }

    /**
     * Get min_price
     *
     * @return integer 
     */
    public function getMinPrice()
    {
        return $this->min_price;
    }

    /**
     * Set max_price
     *
     * @param integer $maxPrice
     * @return Filters
     */
    public function setMaxPrice($maxPrice)
    {
        $this->max_price = $maxPrice;

        return $this;
    }

    /**
     * Get max_price
     *
     * @return integer 
     */
    public function getMaxPrice()
    {
        return $this->max_price;
    }

    /**
     * Set term
     *
     * @param integer $term
     * @return Filters
     */
    public function setTerm($term)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return integer 
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set region_id
     *
     * @param integer $regionId
     * @return Filters
     */
    public function setRegionId($regionId)
    {
        $this->region_id = $regionId;

        return $this;
    }

    /**
     * Get region_id
     *
     * @return integer 
     */
    public function getRegionId()
    {
        return $this->region_id;
    }

    /**
     * Set furniture
     *
     * @param boolean $furniture
     * @return Filters
     */
    public function setFurniture($furniture)
    {
        $this->furniture = $furniture;

        return $this;
    }

    /**
     * Get furniture
     *
     * @return boolean 
     */
    public function getFurniture()
    {
        return $this->furniture;
    }

    /**
     * Set kitchen_furniture
     *
     * @param boolean $kitchenFurniture
     * @return Filters
     */
    public function setKitchenFurniture($kitchenFurniture)
    {
        $this->kitchen_furniture = $kitchenFurniture;

        return $this;
    }

    /**
     * Get kitchen_furniture
     *
     * @return boolean 
     */
    public function getKitchenFurniture()
    {
        return $this->kitchen_furniture;
    }

    /**
     * Set telephone
     *
     * @param boolean $telephone
     * @return Filters
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return boolean 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set tv
     *
     * @param boolean $tv
     * @return Filters
     */
    public function setTv($tv)
    {
        $this->tv = $tv;

        return $this;
    }

    /**
     * Get tv
     *
     * @return boolean 
     */
    public function getTv()
    {
        return $this->tv;
    }

    /**
     * Set refrigerator
     *
     * @param boolean $refrigerator
     * @return Filters
     */
    public function setRefrigerator($refrigerator)
    {
        $this->refrigerator = $refrigerator;

        return $this;
    }

    /**
     * Get refrigerator
     *
     * @return boolean 
     */
    public function getRefrigerator()
    {
        return $this->refrigerator;
    }

    /**
     * Set washing_machine
     *
     * @param boolean $washingMachine
     * @return Filters
     */
    public function setWashingMachine($washingMachine)
    {
        $this->washing_machine = $washingMachine;

        return $this;
    }

    /**
     * Get washing_machine
     *
     * @return boolean 
     */
    public function getWashingMachine()
    {
        return $this->washing_machine;
    }

    /**
     * Set balcony
     *
     * @param boolean $balcony
     * @return Filters
     */
    public function setBalcony($balcony)
    {
        $this->balcony = $balcony;

        return $this;
    }

    /**
     * Get balcony
     *
     * @return boolean 
     */
    public function getBalcony()
    {
        return $this->balcony;
    }

    /**
     * Set pets
     *
     * @param boolean $pets
     * @return Filters
     */
    public function setPets($pets)
    {
        $this->pets = $pets;

        return $this;
    }

    /**
     * Get pets
     *
     * @return boolean 
     */
    public function getPets()
    {
        return $this->pets;
    }

    /**
     * Set children
     *
     * @param boolean $children
     * @return Filters
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return boolean 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set filter_rooms_count_id
     *
     * @param integer $filterRoomsCountId
     * @return Filters
     */
    public function setFilterRoomsCountId($filterRoomsCountId)
    {
        $this->filter_rooms_count_id = $filterRoomsCountId;

        return $this;
    }

    /**
     * Get filter_rooms_count_id
     *
     * @return integer 
     */
    public function getFilterRoomsCountId()
    {
        return $this->filter_rooms_count_id;
    }

    /**
     * Set filter_metro_stations_id
     *
     * @param integer $filterMetroStationsId
     * @return Filters
     */
    public function setFilterMetroStationsId($filterMetroStationsId)
    {
        $this->filter_metro_stations_id = $filterMetroStationsId;

        return $this;
    }

    /**
     * Get filter_metro_stations_id
     *
     * @return integer 
     */
    public function getFilterMetroStationsId()
    {
        return $this->filter_metro_stations_id;
    }
    /**
     * @var \RealEstate\ParserBundle\Entity\FilterRoomsCount
     */
    private $filter_rooms_count;

    /**
     * @var \RealEstate\ParserBundle\Entity\FilterMetroStations
     */
    private $filter_metro_stations;


    /**
     * Set filter_rooms_count
     *
     * @param \RealEstate\ParserBundle\Entity\FilterRoomsCount $filterRoomsCount
     * @return Filters
     */
    public function setFilterRoomsCount(\RealEstate\ParserBundle\Entity\FilterRoomsCount $filterRoomsCount = null)
    {
        $this->filter_rooms_count = $filterRoomsCount;

        return $this;
    }

    /**
     * Get filter_rooms_count
     *
     * @return \RealEstate\ParserBundle\Entity\FilterRoomsCount 
     */
    public function getFilterRoomsCount()
    {
        return $this->filter_rooms_count;
    }

    /**
     * Set filter_metro_stations
     *
     * @param \RealEstate\ParserBundle\Entity\FilterMetroStations $filterMetroStations
     * @return Filters
     */
    public function setFilterMetroStations(\RealEstate\ParserBundle\Entity\FilterMetroStations $filterMetroStations = null)
    {
        $this->filter_metro_stations = $filterMetroStations;

        return $this;
    }

    /**
     * Get filter_metro_stations
     *
     * @return \RealEstate\ParserBundle\Entity\FilterMetroStations 
     */
    public function getFilterMetroStations()
    {
        return $this->filter_metro_stations;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $metro_station_rerions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->filter_rooms_count = new \Doctrine\Common\Collections\ArrayCollection();
        $this->filter_metro_stations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->metro_station_rerions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add filter_rooms_count
     *
     * @param \RealEstate\ParserBundle\Entity\Photo $filterRoomsCount
     * @return Filters
     */
    public function addFilterRoomsCount(\RealEstate\ParserBundle\Entity\Photo $filterRoomsCount)
    {
        $this->filter_rooms_count[] = $filterRoomsCount;

        return $this;
    }

    /**
     * Remove filter_rooms_count
     *
     * @param \RealEstate\ParserBundle\Entity\Photo $filterRoomsCount
     */
    public function removeFilterRoomsCount(\RealEstate\ParserBundle\Entity\Photo $filterRoomsCount)
    {
        $this->filter_rooms_count->removeElement($filterRoomsCount);
    }

    /**
     * Add filter_metro_stations
     *
     * @param \RealEstate\ParserBundle\Entity\FilterMetroStations $filterMetroStations
     * @return Filters
     */
    public function addFilterMetroStation(\RealEstate\ParserBundle\Entity\FilterMetroStations $filterMetroStations)
    {
        $this->filter_metro_stations[] = $filterMetroStations;

        return $this;
    }

    /**
     * Remove filter_metro_stations
     *
     * @param \RealEstate\ParserBundle\Entity\FilterMetroStations $filterMetroStations
     */
    public function removeFilterMetroStation(\RealEstate\ParserBundle\Entity\FilterMetroStations $filterMetroStations)
    {
        $this->filter_metro_stations->removeElement($filterMetroStations);
    }

    /**
     * Add metro_station_rerions
     *
     * @param \RealEstate\ParserBundle\Entity\MetroStationRegions $metroStationRerions
     * @return Filters
     */
    public function addMetroStationRerion(\RealEstate\ParserBundle\Entity\MetroStationRegions $metroStationRerions)
    {
        $this->metro_station_rerions[] = $metroStationRerions;

        return $this;
    }

    /**
     * Remove metro_station_rerions
     *
     * @param \RealEstate\ParserBundle\Entity\MetroStationRegions $metroStationRerions
     */
    public function removeMetroStationRerion(\RealEstate\ParserBundle\Entity\MetroStationRegions $metroStationRerions)
    {
        $this->metro_station_rerions->removeElement($metroStationRerions);
    }

    /**
     * Get metro_station_rerions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMetroStationRerions()
    {
        return $this->metro_station_rerions;
    }
    /**
     * @var \DateTime
     */
    private $created_at;


    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Filters
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $metro_station_regions;

    /**
     * @var \RealEstate\ParserBundle\Entity\User
     */
    private $user;


    /**
     * Add metro_station_regions
     *
     * @param \RealEstate\ParserBundle\Entity\MetroStationRegions $metroStationRegions
     * @return Filters
     */
    public function addMetroStationRegion(\RealEstate\ParserBundle\Entity\MetroStationRegions $metroStationRegions)
    {
        $this->metro_station_regions[] = $metroStationRegions;

        return $this;
    }

    /**
     * Remove metro_station_regions
     *
     * @param \RealEstate\ParserBundle\Entity\MetroStationRegions $metroStationRegions
     */
    public function removeMetroStationRegion(\RealEstate\ParserBundle\Entity\MetroStationRegions $metroStationRegions)
    {
        $this->metro_station_regions->removeElement($metroStationRegions);
    }

    /**
     * Get metro_station_regions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMetroStationRegions()
    {
        return $this->metro_station_regions;
    }

    /**
     * Set user
     *
     * @param \RealEstate\ParserBundle\Entity\User $user
     * @return Filters
     */
    public function setUser(\RealEstate\ParserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RealEstate\ParserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var boolean
     */
    private $is_active;


    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Filters
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }
}
