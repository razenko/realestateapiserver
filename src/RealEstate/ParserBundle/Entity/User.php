<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 */
class User extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;

    public function __construct()
    {
    	parent::__construct();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $mobile_regs;


    /**
     * Add mobile_regs
     *
     * @param \RealEstate\ParserBundle\Entity\Mobilereg $mobileRegs
     * @return User
     */
    public function addMobileReg(\RealEstate\ParserBundle\Entity\Mobilereg $mobileRegs)
    {
        $this->mobile_regs[] = $mobileRegs;

        return $this;
    }

    /**
     * Remove mobile_regs
     *
     * @param \RealEstate\ParserBundle\Entity\Mobilereg $mobileRegs
     */
    public function removeMobileReg(\RealEstate\ParserBundle\Entity\Mobilereg $mobileRegs)
    {
        $this->mobile_regs->removeElement($mobileRegs);
    }

    /**
     * Get mobile_regs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMobileRegs()
    {
        return $this->mobile_regs;
    }

    /**
     * @var \RealEstate\ParserBundle\Entity\UserSettings
     */
    private $settings;

    /**
     * Add settings
     *
     * @param \RealEstate\ParserBundle\Entity\UserSettings $settings
     * @return User
     */
    public function setSettings(\RealEstate\ParserBundle\Entity\UserSettings $settings)
    {
        $this->settings = $settings;
    }

    /**
     * Get settings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @var \RealEstate\ParserBundle\Entity\TariffSubscription
     */
    private $tariff_subscription;

    /**
     * @var \RealEstate\ParserBundle\Entity\TariffTransaction
     */
    private $tariff_transactions;


    /**
     * Set tariff_subscription
     *
     * @param \RealEstate\ParserBundle\Entity\TariffSubscription $tariffSubscription
     * @return User
     */
    public function setTariffSubscription(\RealEstate\ParserBundle\Entity\TariffSubscription $tariffSubscription = null)
    {
        $this->tariff_subscription = $tariffSubscription;

        return $this;
    }

    /**
     * Get tariff_subscription
     *
     * @return \RealEstate\ParserBundle\Entity\TariffSubscription 
     */
    public function getTariffSubscription()
    {
        return $this->tariff_subscription;
    }

    /**
     * Set tariff_transactions
     *
     * @param \RealEstate\ParserBundle\Entity\TariffTransaction $tariffTransactions
     * @return User
     */
    public function setTariffTransactions(\RealEstate\ParserBundle\Entity\TariffTransaction $tariffTransactions = null)
    {
        $this->tariff_transactions = $tariffTransactions;

        return $this;
    }

    /**
     * Get tariff_transactions
     *
     * @return \RealEstate\ParserBundle\Entity\TariffTransaction 
     */
    public function getTariffTransactions()
    {
        return $this->tariff_transactions;
    }

    /**
     * Add tariff_transactions
     *
     * @param \RealEstate\ParserBundle\Entity\TariffTransaction $tariffTransactions
     * @return User
     */
    public function addTariffTransaction(\RealEstate\ParserBundle\Entity\TariffTransaction $tariffTransactions)
    {
        $this->tariff_transactions[] = $tariffTransactions;

        return $this;
    }

    /**
     * Remove tariff_transactions
     *
     * @param \RealEstate\ParserBundle\Entity\TariffTransaction $tariffTransactions
     */
    public function removeTariffTransaction(\RealEstate\ParserBundle\Entity\TariffTransaction $tariffTransactions)
    {
        $this->tariff_transactions->removeElement($tariffTransactions);
    }

    /**
     * @var \RealEstate\ParserBundle\Entity\Filters
     */
    private $filters;

    /**
     * Add filters
     *
     * @param \RealEstate\ParserBundle\Entity\Filters $filters
     * @return User
     */
    public function addFilter(\RealEstate\ParserBundle\Entity\Filters $filters)
    {
        $this->filters[] = $filters;

        return $this;
    }

    /**
     * Remove filters
     *
     * @param \RealEstate\ParserBundle\Entity\Filters $filters
     */
    public function removeFilter(\RealEstate\ParserBundle\Entity\Filters $filters)
    {
        $this->filters->removeElement($filters);
    }

    /**
     * Get filters
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFilters()
    {
        return $this->filters;
    }
}
