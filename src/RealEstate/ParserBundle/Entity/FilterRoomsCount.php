<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FilterRoomsCount
 */
class FilterRoomsCount
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var integer
     */
    private $value;

    /**
     * @var \RealEstate\ParserBundle\Entity\Filters
     */
    private $filters;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return FilterRoomsCount
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set value
     *
     * @param integer $value
     * @return FilterRoomsCount
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return integer 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set filters
     *
     * @param \RealEstate\ParserBundle\Entity\Filters $filters
     * @return FilterRoomsCount
     */
    public function setFilters(\RealEstate\ParserBundle\Entity\Filters $filters = null)
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * Get filters
     *
     * @return \RealEstate\ParserBundle\Entity\Filters 
     */
    public function getFilters()
    {
        return $this->filters;
    }
    /**
     * @var integer
     */
    private $filter_id;


    /**
     * Set filter_id
     *
     * @param integer $filterId
     * @return FilterRoomsCount
     */
    public function setFilterId($filterId)
    {
        $this->filter_id = $filterId;

        return $this;
    }

    /**
     * Get filter_id
     *
     * @return integer 
     */
    public function getFilterId()
    {
        return $this->filter_id;
    }
    /**
     * @var \RealEstate\ParserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \RealEstate\ParserBundle\Entity\User $user
     * @return FilterRoomsCount
     */
    public function setUser(\RealEstate\ParserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RealEstate\ParserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
