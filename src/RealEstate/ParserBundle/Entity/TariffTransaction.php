<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TariffTransaction
 */
class TariffTransaction
{
    const STATUS_INITIALIZED = 1;
    const STATUS_SUCCESSED = 2;
    const STATUS_REJECTED = 3;
    const STATUS_FAILED = 4;
    
    const PLATRON_PAYSYSTEM = 1;
    const IOS_IAP_PAYSYSTEM = 2;
    
    const RESULT_FAILED = 0;
    
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var integer
     */
    private $tariff_id;

    /**
     * @var string
     */
    private $paysystem;

    /**
     * @var float
     */
    private $price;

    /**
     * @var integer
     */
    private $term;

    /**
     * @var float
     */
    private $total;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var integer
     */
    private $success;

    /**
     * @var integer
     */
    private $error_code;

    /**
     * @var string
     */
    private $error_message;
    
    
    
    public function isInitialized()
    {
        return $this->status == self::STATUS_INITIALIZED;
    }
    
    public function isSuccessed()
    {
        return $this->status == self::STATUS_SUCCESSED;
    }
    
    public function isFailed()
    {
        return $this->status == self::STATUS_FAILED;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return TariffTransaction
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set tariff_id
     *
     * @param integer $tariffId
     * @return TariffTransaction
     */
    public function setTariffId($tariffId)
    {
        $this->tariff_id = $tariffId;

        return $this;
    }

    /**
     * Get tariff_id
     *
     * @return integer 
     */
    public function getTariffId()
    {
        return $this->tariff_id;
    }

    /**
     * Set paysystem
     *
     * @param string $paysystem
     * @return TariffTransaction
     */
    public function setPaysystem($paysystem)
    {
        $this->paysystem = $paysystem;

        return $this;
    }

    /**
     * Get paysystem
     *
     * @return string 
     */
    public function getPaysystem()
    {
        return $this->paysystem;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return TariffTransaction
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set term
     *
     * @param integer $term
     * @return TariffTransaction
     */
    public function setTerm($term)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return integer 
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return TariffTransaction
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return TariffTransaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set success
     *
     * @param integer $success
     * @return TariffTransaction
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get success
     *
     * @return integer 
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * Set error_code
     *
     * @param integer $errorCode
     * @return TariffTransaction
     */
    public function setErrorCode($errorCode)
    {
        $this->error_code = $errorCode;

        return $this;
    }

    /**
     * Get error_code
     *
     * @return integer 
     */
    public function getErrorCode()
    {
        return $this->error_code;
    }

    /**
     * Set error_message
     *
     * @param string $errorMessage
     * @return TariffTransaction
     */
    public function setErrorMessage($errorMessage)
    {
        $this->error_message = $errorMessage;

        return $this;
    }

    /**
     * Get error_message
     *
     * @return string 
     */
    public function getErrorMessage()
    {
        return $this->error_message;
    }
    /**
     * @var \RealEstate\ParserBundle\Entity\User
     */
    private $user;

    /**
     * @var \RealEstate\ParserBundle\Entity\Tariff
     */
    private $tariff;

    /**
     * Set user
     *
     * @param \RealEstate\ParserBundle\Entity\User $user
     * @return TariffTransaction
     */
    public function setUser(\RealEstate\ParserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RealEstate\ParserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set tariff
     *
     * @param \RealEstate\ParserBundle\Entity\Tariff $tariff
     * @return TariffTransaction
     */
    public function setTariff(\RealEstate\ParserBundle\Entity\Tariff $tariff = null)
    {
        $this->tariff = $tariff;

        return $this;
    }

    /**
     * Get tariff
     *
     * @return \RealEstate\ParserBundle\Entity\Tariff 
     */
    public function getTariff()
    {
        return $this->tariff;
    }

    /**
     * @var integer
     */
    private $active_tariff_id;


    /**
     * Set active_tariff_id
     *
     * @param integer $activeTariffId
     * @return TariffTransaction
     */
    public function setActiveTariffId($activeTariffId)
    {
        $this->active_tariff_id = $activeTariffId;

        return $this;
    }

    /**
     * Get active_tariff_id
     *
     * @return integer 
     */
    public function getActiveTariffId()
    {
        return $this->active_tariff_id;
    }
    /**
     * @var integer
     */
    private $tariff_subscription_id;

    /**
     * @var \RealEstate\ParserBundle\Entity\TariffSubscription
     */
    private $tariff_subscription;


    /**
     * Set tariff_subscription_id
     *
     * @param integer $tariffSubscriptionId
     * @return TariffTransaction
     */
    public function setTariffSubscriptionId($tariffSubscriptionId)
    {
        $this->tariff_subscription_id = $tariffSubscriptionId;

        return $this;
    }

    /**
     * Get tariff_subscription_id
     *
     * @return integer 
     */
    public function getTariffSubscriptionId()
    {
        return $this->tariff_subscription_id;
    }

    /**
     * Set tariff_subscription
     *
     * @param \RealEstate\ParserBundle\Entity\TariffSubscription $tariffSubscription
     * @return TariffTransaction
     */
    public function setTariffSubscription(\RealEstate\ParserBundle\Entity\TariffSubscription $tariffSubscription = null)
    {
        $this->tariff_subscription = $tariffSubscription;

        return $this;
    }

    /**
     * Get tariff_subscription
     *
     * @return \RealEstate\ParserBundle\Entity\TariffSubscription 
     */
    public function getTariffSubscription()
    {
        return $this->tariff_subscription;
    }
    /**
     * @var integer
     */
    private $closed;


    /**
     * Set closed
     *
     * @param integer $closed
     * @return TariffTransaction
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Get closed
     *
     * @return integer 
     */
    public function getClosed()
    {
        return $this->closed;
    }
    /**
     * @var \DateTime
     */
    private $created_at;

    /**
     * @var \DateTime
     */
    private $updeted_at;


    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return TariffTransaction
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updeted_at
     *
     * @param \DateTime $updetedAt
     * @return TariffTransaction
     */
    public function setUpdetedAt($updetedAt)
    {
        $this->updeted_at = $updetedAt;

        return $this;
    }

    /**
     * Get updeted_at
     *
     * @return \DateTime 
     */
    public function getUpdetedAt()
    {
        return $this->updeted_at;
    }
    /**
     * @var \DateTime
     */
    private $updated_at;


    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return TariffTransaction
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
    /**
     * @var integer
     */
    private $payment_id;


    /**
     * Set payment_id
     *
     * @param integer $paymentId
     * @return TariffTransaction
     */
    public function setPaymentId($paymentId)
    {
        $this->payment_id = $paymentId;

        return $this;
    }

    /**
     * Get payment_id
     *
     * @return integer 
     */
    public function getPaymentId()
    {
        return $this->payment_id;
    }
}
