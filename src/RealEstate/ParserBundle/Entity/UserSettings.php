<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserSettings
 */
class UserSettings
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var boolean
     */
    private $push_subscription;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return UserSettings
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set push_subscription
     *
     * @param boolean $pushSubscription
     * @return UserSettings
     */
    public function setPushSubscription($pushSubscription)
    {
        $this->push_subscription = $pushSubscription;

        return $this;
    }

    /**
     * Get push_subscription
     *
     * @return boolean 
     */
    public function getPushSubscription()
    {
        return $this->push_subscription;
    }
    /**
     * @var \RealEstate\ParserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \RealEstate\ParserBundle\Entity\User $user
     * @return UserSettings
     */
    public function setUser(\RealEstate\ParserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \RealEstate\ParserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var smallinteger
     */
    private $user_type_id;


    /**
     * Set user_type_id
     *
     * @param integer $userTypeId
     * @return UserSettings
     */
    public function setUserTypeId($userTypeId)
    {
        $this->user_type_id = $userTypeId;

        return $this;
    }

    /**
     * Get user_type_id
     *
     * @return \smallinteger 
     */
    public function getUserTypeId()
    {
        return $this->user_type_id;
    }
}
