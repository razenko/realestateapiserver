<?php

namespace RealEstate\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RealtyType
 */
class RealtyType
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $main_type_id;

    /**
     * @var integer
     */
    private $sub_type_id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set main_type_id
     *
     * @param integer $mainTypeId
     * @return RealtyType
     */
    public function setMainTypeId($mainTypeId)
    {
        $this->main_type_id = $mainTypeId;

        return $this;
    }

    /**
     * Get main_type_id
     *
     * @return integer 
     */
    public function getMainTypeId()
    {
        return $this->main_type_id;
    }

    /**
     * Set sub_type_id
     *
     * @param integer $subTypeId
     * @return RealtyType
     */
    public function setSubTypeId($subTypeId)
    {
        $this->sub_type_id = $subTypeId;

        return $this;
    }

    /**
     * Get sub_type_id
     *
     * @return integer 
     */
    public function getSubTypeId()
    {
        return $this->sub_type_id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RealtyType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
