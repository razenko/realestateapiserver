<?php

namespace RealEstate\ParserBundle\Payment;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of Platron
 *
 * @author alex
 */
class Platron {
    private $config;
    private $secret_key;
    private $script_name;
    private $params;
    
    
    
    public function __construct(array $config, $script_url, array $params)
    {
        $this->config = $config;
        $this->secret_key = $config['secret_key'];
        $this->script_name = $this->getScriptName($script_url);
        
        unset($params['pg_sig']);
        $this->params = $params;
    }
    
    public function checkSig($sig)
    {
        return (string)$sig === $this->generateSig();
    }
    
    public function generateSig()
    {
        $paramsToCheck = $this->params;
        
        ksort($paramsToCheck);
        array_unshift($paramsToCheck, $this->script_name);
        array_push($paramsToCheck, $this->secret_key);
        $raw_sig = implode(';', $paramsToCheck);

        return md5($raw_sig);
    }
    
    private function getScriptName($script_url)
    {
        $start_index = strrpos($script_url, "/") + 1;
        $end_index = strpos($script_url, "?");
	
        if ($end_index !== false)
            return substr($script_url, $start_index, $end_index - $start_index);
        else
            return substr($script_url, $start_index);
    }
}
