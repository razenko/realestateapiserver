<?php

namespace RealEstate\ParserBundle\Mapper;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DomCrawler\Crawler;
use Doctrine\ORM\EntityManager;
use RealEstate\ParserBundle\Entity\Realty;
use RealEstate\ParserBundle\Entity\Photo;
use RealEstate\ParserBundle\Entity\ParserResult;
use RealEstate\ParserBundle\Entity\UserData;
use JMS\DiExtraBundle\Annotation as DI;
use RealEstate\ParserBundle\PushNotification\Messenger;
use Goutte\Client;


/**
 * @DI\Service("real_estate.parser.mapper", public=true)
 */

class RealtyMapper extends ContainerAware
{
    CONST BASE_URL = 'http://www.cian.ru';
    public $users;
    public $newUserRealties;
    private $config;
    private $region;
    
    
    /**
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager")
     * })
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->users = array();
        $this->newUserRealties = array();
    }
    
    public function setConfig(array $config)
    {
        $this->config = $config;
    }
    
    public function map(Crawler $crawler, $region)
    {
        $em = $this->em;
        $this->region = $region;
        $realtyParsedArray = array();
        $realtiesRemoteIdArray = array();
        $client = new Client();
        $total_parsed = 0;
        $parsed_realties_count = 0;
        
        $pageNavBlock = $crawler->filter('td#pagenav_top');
        preg_match('#\d+#', $pageNavBlock->text(), $match) ? $total_realties_count = $match[0] : $total_realties_count = 0;
        
        $realtyTable = $crawler->filter('table.cat tr');
        $realtyTableCount = $realtyTable->count();
        
        if ($realtyTableCount <= 0)
            return false;
        
        for ($i = 0; $i < $realtyTableCount-2; $i++)
        {
            $realtyItem = $realtyTable->eq($i+2);                               
            
            if($realtyItem->filter('td')->eq(1)->getNode(0) == NULL)                    
                continue;                
            
            //FIND COMMENT BLOCK FOR DEATIL INFO LINK
            $realtyCommentBlock = $realtyItem->filter('td')->eq(9);
            $moreInfo = $realtyCommentBlock->filter('div[align=right] a');
            
            $realtiesUrl[] = self::BASE_URL . $moreInfo->attr('href');                
        }
        
        if (!empty($realtiesUrl))
        {
            $max_iteration = $this->config['iterations']['max_per_page'];
            
            foreach ($realtiesUrl as $key => $realtyUrl) 
            {
                $is_parsed = true;
                
                for ($i = 0; $i < $max_iteration; $i++)
                {
                    echo $key . '. Trying to parse --> ' . $realtyUrl . PHP_EOL;
                    $curl_error = false;
                    
                    try
                    {
                        $crawler = $client->request('GET', $realtyUrl);
                    }
                    catch (\Exception $exc)
                    {
                        $curl_error = true;
                    }
                    
                    if ($curl_error || $client->getResponse()->getStatus() >= 400)
                    {
                        if ($i === $max_iteration - 1)
                            $is_parsed = false;
                        else
                            continue;
                    }
                    
                    break;
                }
                
                if (!$is_parsed || $crawler->filter('.object_descr_warnings_inline')->count() > 0)
                    continue;
                
                $advertisementData = $this->parseAdvertisementData($crawler);
                
                if (!$advertisementData)
                    continue;
                
                $realtyParsedArray[] = $advertisementData;
                $realtiesRemoteIdArray[] = $advertisementData['remote_id'];
                $parsed_realties_count++;
            }
        }
        
        $realties = $em->getRepository('RealEstate\ParserBundle\Entity\Realty')
            ->getRealtiesByRemoteId($realtiesRemoteIdArray);
            
        echo '----------------------------------------Save realties-------------------------------------------'.PHP_EOL;

        $added_to_db = 0;
        $updated = 0;
        $parsedRealtyRemoteIds = array();
        
        foreach ($realtyParsedArray as $parsed)
        {
            $is_exist = false;

            if (isset($parsedRealtyRemoteIds[$parsed['remote_id']]))
                continue;

            foreach ($realties as $remote)
            {
                if ($parsed['remote_id'] != $remote->getRemoteId())
                    continue;

                $is_exist = true;

                if ($parsed['hash'] != $remote->getHash())
                {
                    $updated++;
                    $this->assertObject($remote, $parsed);
                    $this->checkUserFilters($parsed);
                    $parsedRealtyRemoteIds[$parsed['remote_id']] = true;
                }
            }

            if (!$is_exist)
            {
                $added_to_db++;
                $this->assertObject(new Realty, $parsed);
                $this->checkUserFilters($parsed);
                $parsedRealtyRemoteIds[$parsed['remote_id']] = true;
            }
        }

        $this->assertParserResult($total_realties_count, $parsed_realties_count, $added_to_db, $updated);
        $this->sendMessages();

        echo 'Total count realties is: ' . $total_realties_count . PHP_EOL;
        echo 'Total count parsed row is: ' . $parsed_realties_count . PHP_EOL;
        echo 'Added in DB is: ' . $added_to_db . PHP_EOL;
        echo 'Updated: ' . $updated . PHP_EOL;
        
        $em->flush();
        
        return true;
    }
    
    public function parseAdvertisementData($crawler)
    {
        $data = array();
        $description_block_index = 4;
        
        if ($this->region->getHasMetro())
            $description_block_index = 5;
        
        $data['region_id'] = $this->region->getId();
        $remoteId = str_replace('offer_', '', $crawler->filter('div.offer_container')->attr('id'));
        $data['remote_id'] = $remoteId;

        $title = $crawler->filter('div.object_descr_title')->text();
        $data['title'] = $title;

        //FIND LOCATION
        $address = $crawler->filter('h1.object_descr_addr')->text();
        $metro = '';

        if ($this->region->getHasMetro())
        {
            $metroNode = $crawler->filter('div.object_descr_metro > span');

            // бывают объявления где явно не указана станция метро
            // такие объявления нужно пропускать, т.к. в них отличается html структура
            if ($metroNode->count() === 0)
                return null;

            // на cian.ru изменили html в районе станции метро (убрали запятую и мб добавили span)
            $metroData = explode("\n", trim($metroNode->text()));
            $metro = $metroData[0];
        }

        $data['location'] = array(
            'address' => $address,
            'metro' => $metro
        );

        //FIND PRICE AND ROOMS COUNT
        $descriptionPriceBlock = $crawler->filter('td.object_descr_td_l div')->eq($description_block_index);               
        preg_match('#(\d+) мес\.#', $descriptionPriceBlock->text(), $match) ? $milestone = $match[1] : $milestone = null;
        preg_match('#депозита#', $descriptionPriceBlock->text(), $match) ? $desposit = false : $deposit = true;
        $amount = $crawler->filter('#price_rur')->text();
        preg_match('#(\d)-комн#', $title, $match) ? $rooms_count = $match[1] : $rooms_count = 0;

        $term = 0;
        if (preg_match('#посуточно#', $title))
            $term = 1;
        elseif (preg_match('#меньше года#', $title))
            $term = 3;
        elseif (preg_match('#от года#', $title))
            $term = 4;

        $payment = $crawler->filter('#price_rur + div')->text();

        $data['price'] = array(
            'rooms_count' => $rooms_count,
            'amount' => $amount,
            'payment' => $payment,
            'milestone' => $milestone,
            'term' => $term
        );

        //FIND APPARTAMENTS
        if ($additionDescription = $crawler->filter('.object_descr_details')->getNode(0) == null)
        {
            $furniture = false; $kitchen_furniture = false; $telephone = false; $tv = false; $washing_machine = false; 
            $refrigerator = false; $balcony = false; $pets = false; $children = false;
        }
        else
        {
            $additionDescription = $crawler->filter('.object_descr_details');
            preg_match('#жил\.мебель#', $additionDescription->text()) ? $furniture = true : $furniture = false;
            preg_match('#кух\.мебель#', $additionDescription->text()) ? $kitchen_furniture = true : $kitchen_furniture = false;
            preg_match('#телефона#', $additionDescription->text()) ? $telephone = false : $telephone = true;
            preg_match('#ТВ#', $additionDescription->text()) ? $tv = true : $tv = false;
            preg_match('#стир\.машина#', $additionDescription->text()) ? $washing_machine = true : $washing_machine = false;
            preg_match('#холодильник#', $additionDescription->text()) ? $refrigerator = true : $refrigerator = false;
            preg_match('#балкон#', $additionDescription->text()) ? $balcony = true : $balcony = false;
            preg_match('#животными#', $additionDescription->text()) ? $pets = true : $pets = false;
            preg_match('#детьми#', $additionDescription->text()) ? $children = true : $children = false;
        }

        $data['additionalDescription'] = array(
            'furniture' => $furniture,
            'kitchen_furniture' => $kitchen_furniture,
            'telephone' => $telephone,
            'tv' => $tv,
            'washing_machine' => $washing_machine,
            'refrigerator' => $refrigerator,
            'balcony' => $balcony,
            'pets' => $pets,
            'children' => $children,
        );

        //FIND CONTACTS                    
        $phones = $crawler->filter('strong.object_descr_phone_orig');

        $contactPhonesArray = explode(",", $phones->text());

        $data['contacts']['phone1'] = $contactPhonesArray[0];
        isset($contactPhonesArray[1]) ? $data['contacts']['phone2'] = $contactPhonesArray[1] : $data['contacts']['phone2'] = null;

        //FIND AREA    
        $commonInfo = $crawler->filter('table.object_descr_props');

        //commonArea
        $commonAreaBlock = $commonInfo->filter('tr')->eq(2)->filter('td');
        preg_match('#\d+#', $commonAreaBlock->text(), $match) ? $commonArea = $match[0] : $commonArea = null;
        //roomsArea
        $roomsAreaBlock = $commonInfo->filter('tr')->eq(3)->filter('td');
        preg_match('#\d+#', $roomsAreaBlock->text(), $match) ? $roomsArea = $match[0] : $roomsArea = null;
        //livingArea
        $livingAreaBlock = $commonInfo->filter('tr')->eq(4)->filter('td');
        preg_match('#\d+#', $livingAreaBlock->text(), $match) ? $livingArea = $match[0] : $livingArea = null;
        //kitchenArea
        $kitchenAreaBlock = $commonInfo->filter('tr')->eq(5)->filter('td');
        preg_match('#\d+#', $kitchenAreaBlock->text(), $match) ? $kitchenArea = $match[0] : $kitchenArea = null;

        $data['area'] = array(
            'common'    => $commonArea,
            'rooms'     => $roomsArea,
            'living'    => $livingArea,
            'kitchen'   => $kitchenArea
        );

        //FIND COMMENTS
        $comments = $crawler->filter('div.object_descr_text');
        $phones = $comments->filter('div.object_descr_phones');
        $comment = trim(str_replace($phones->text(), '', $comments->text()));
        $data['comment'] = $comment;

        //FIND PERCENTAGE
        $percentageBlock = $crawler->filter('td.object_descr_td_l div')->eq($description_block_index);
        preg_match('#комиссия (\d+)#', $percentageBlock->text(), $match);
        isset($match[1]) ? $percentage = $match[1] : $percentage = null;
        $data['percentage'] = $percentage;

        //FIND FLOORS
        $floors = $crawler->filter('table.object_descr_props tr')->eq(1)->filter('td');
        $data['floors'] = $floors->text();

        //FIND MAP COORDS
        $coords = $crawler->filter('div.object_descr_map_static input')->attr('value');
        parse_str($coords);
        $coords_arr = explode(',' , $pt);

        $data['latitude'] = $coords_arr[0];
        $data['longitude'] = $coords_arr[1];

        //Clear empty lines in text fields
        $this->makeOneLineString($data['location']['address']);
        $this->makeOneLineString($data['location']['metro']);
        $this->makeOneLineString($data['title']);
        $this->makeOneLineString($data['price']['payment']);
        $this->makeOneLineString($data['contacts']['phone1']);
        $this->makeOneLineString($data['contacts']['phone2']);

        //FIND PHOTOS
        $photos = $crawler->filter('div.object_descr_images_w a');
        $photosArray = array();
        $data['photos'] = null;

        if ($photos->count() > 0)
        {
            for($j = 0; $j < $photos->count(); $j++)
                $photosArray[$j] = $photos->eq($j)->attr('href');

            $data['photos'] = $photosArray;
        }

        $hash = $this->generateHash($data);
        $data['hash'] = $hash;
        
        return $data;
    }
    
    public function assertObject(Realty $realty, Array $parsed) 
    {
        $realty->setRemoteId($parsed['remote_id']);
        $realty->setRegion($this->region);
        $realty->setRoomsCount($parsed['price']['rooms_count']);
        $realty->setTitle($parsed['title']);
        $realty->setMetro($parsed['location']['metro']);
        $realty->setHash($parsed['hash']);
        $realty->setPrice($parsed['price']['amount']);
        $realty->setPayment($parsed['price']['payment']);
        $realty->setMilestone($parsed['price']['milestone']);
        $realty->setTerm($parsed['price']['term']);
        $realty->setFurniture($parsed['additionalDescription']['furniture']);
        $realty->setKitchenFurniture($parsed['additionalDescription']['kitchen_furniture']);
        $realty->setTelephone($parsed['additionalDescription']['telephone']);
        $realty->setTv($parsed['additionalDescription']['tv']);
        $realty->setRefrigerator($parsed['additionalDescription']['refrigerator']);
        $realty->setWashingMachine($parsed['additionalDescription']['washing_machine']);
        $realty->setBalcony($parsed['additionalDescription']['balcony']);
        $realty->setPets($parsed['additionalDescription']['pets']);
        $realty->setChildren($parsed['additionalDescription']['children']);
        $realty->setContactPhone1($parsed['contacts']['phone1']);
        $realty->setContactPhone2($parsed['contacts']['phone2']);
        $realty->setComment($parsed['comment']);
        $realty->setFloors($parsed['floors']);
        $realty->setLatitude($parsed['latitude']);
        $realty->setLongitude($parsed['longitude']);
        $realty->setPercentage($parsed['percentage']);
        $realty->setAddress($parsed['location']['address']);
        $realty->setCommonArea($parsed['area']['common']);
        $realty->setRoomsArea($parsed['area']['rooms']);
        $realty->setLivingArea($parsed['area']['living']);
        $realty->setKitchenArea($parsed['area']['kitchen']);
        
        //if Realty is new add photos
        if (!$realty->getId() && $parsed['photos'])
        {
            foreach ($parsed['photos'] as $photoUrl)
            {
                $photo = new Photo();
                $photo->setUrl($photoUrl);
                $photo->setRealty($realty);
                $realty->addPhoto($photo);
            }
        }
        else 
        {
            //if Realty is exist, remove photos and add again
            $photos = $realty->getPhotos();
            
            foreach ($photos as $photo)
                $realty->removePhoto($photo);
            
            if ($parsed['photos'])
            {
                foreach ($parsed['photos'] as $photoUrl)
                {
                    $photo = new Photo();
                    $photo->setUrl($photoUrl);
                    $photo->setRealty($realty);
                    $realty->addPhoto($photo);
                }
            }
        }
        
        $this->em->persist($realty);
        
        return true;
    }
    
    public function checkUserFilters(Array $parsed)
    {
        $users = $this->em
            ->getRepository('RealEstate\ParserBundle\Entity\Filters')
            ->getUsersByFilters($parsed);
        
        foreach ($users as $user)
        {
            if (isset($this->newUserRealties[$user['user_id']]))
                $this->newUserRealties[$user['user_id']]++;
            else
                $this->newUserRealties[$user['user_id']] = 1;
        }
    }
    
    public function generateHash(Array $a)
    {
        $hash = '';
        $hash .= $a['remote_id'];
        $hash .= $a['percentage'];
        $hash .= $a['floors'];
        $hash .= $a['title'];
        $hash .= $a['additionalDescription']['furniture'];
        $hash .= $a['additionalDescription']['kitchen_furniture'];
        $hash .= $a['additionalDescription']['telephone'];
        $hash .= $a['additionalDescription']['tv'];
        $hash .= $a['additionalDescription']['washing_machine'];
        $hash .= $a['additionalDescription']['refrigerator'];
        $hash .= $a['additionalDescription']['balcony'];
        $hash .= $a['additionalDescription']['pets'];
        $hash .= $a['additionalDescription']['children'];
        $hash .= $a['contacts']['phone1'];
        $hash .= $a['location']['address'];
        $hash .= $a['longitude'];
        $hash .= $a['latitude'];
        $hash .= $a['location']['metro'];
        $hash .= $a['price']['amount'];
        $hash .= $a['price']['milestone'];
        $hash .= $a['price']['payment'];
        $hash .= $a['price']['term'];
        $hash .= $a['area']['common'];
        $hash .= $a['area']['rooms'];
        $hash .= $a['area']['living'];
        $hash .= $a['area']['kitchen'];
        $hash .= $a['comment'];
        
        if ($a['photos'])
        {
            foreach ($a['photos'] as $photo)
                $hash .= $photo;
        }
        
        return sha1($hash);
    }
    
    public function sendMessages()
    {
        if ($this->config['bench_mode'])
            return $this->immitatePushNotificationDistribution();
        
    	if (empty($this->newUserRealties))
            return;
        
        $userData = $this->em->getRepository('RealEstateParserBundle:UserData')
            ->findOneById(UserData::REALTY_ID);
        
        foreach($this->newUserRealties as $user_id => $value)
        {
            $user = $this->em
                ->getRepository('RealEstateParserBundle:User')
                ->findOneById($user_id);
            
            $this->em->getRepository('RealEstateParserBundle:UserDataState')
                ->setChangedState($user, $userData);
            
            $pushTokenRecords = $this->em
                ->getRepository('RealEstateParserBundle:MobileReg')
                ->getMobilePushTokensByUserId($user_id, true);
            
            if (!count($pushTokenRecords))
                continue;
            
            $pushNotificationMessenger = new Messenger($this->container);
            $pushNotificationMessenger->setPushTokenRecords($pushTokenRecords);
            
            $message = '1 новое объявление пришло';
            
            if ($value > 1)
                $message = $value . ' новых объявлений пришло';
            
            $data = array(
                'badge' => $value,
                'callback' => 'updateMyTape'
            );
            
            $pushNotificationMessenger->sendMessage($message, $data);
        }
    }
    
    public function assertParserResult($total, $parsed, $added, $updated)
    {
        $parserResult = new ParserResult();
        
        $parserResult->setTotalCount($total)
            ->setParsedCount($parsed)
            ->setAddedCount($added)
            ->setUpdatedCount($updated);
                
        $this->em->persist($parserResult);        
        
        return true;
    }
    
    private function immitatePushNotificationDistribution()
    {
        for ($i = 0; $i < 100000; $i++)
        {
            $user_agent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; ru) Opera 8.50')";
            $referer = 'http://www.yandex.ru/';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://yandex.ru/yandsearch?text=tests&lr=47');
            curl_setopt($ch, CURLOPT_REFERER, $referer);
            curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

            $response = curl_exec($ch);
            curl_close($ch);
        }
    }
    
    private function removeBlankLines(&$string)
    {
        $parts = explode(PHP_EOL, $string);
        $string = '';

        foreach ($parts as $line)
        {
            if (!trim($line))
                continue;

            if ($string)
                $string .= PHP_EOL;

            $string .= trim($line);
        }
    }
    
    private function makeOneLineString(&$string)
    {
        $parts = explode(PHP_EOL, $string);
        $string = '';

        foreach ($parts as $line)
        {
            if (!trim($line))
                continue;

            if ($string)
                $string .= ' ';

            $string .= trim($line);
        }
    }
}
