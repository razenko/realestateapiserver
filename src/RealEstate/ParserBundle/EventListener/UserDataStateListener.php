<?php

namespace RealEstate\ParserBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use RealEstate\ParserBundle\Entity\UserData;
use RealEstate\ParserBundle\Entity\UserSettings;
use RealEstate\ParserBundle\Entity\TariffSubscription;

/**
 * Description of UserDataStateListener
 *
 * @author alex
 */
class UserDataStateListener
{
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->postSave($args);
    }
    
    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->postSave($args);
    }
    
    private function postSave(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        // perhaps you only want to act on some "Product" entity
        if ($entity instanceof UserSettings)
        {
            $user = $entity->getUser();
            $userData = $em->getRepository('RealEstateParserBundle:UserData')
                ->findOneById(UserData::USER_SETTINGS_ID);
            
            $em->getRepository('RealEstateParserBundle:UserDataState')
                ->setChangedState($user, $userData);
        } elseif ($entity instanceof TariffSubscription)
        {
            $user = $entity->getUser();
            $userData = $em->getRepository('RealEstateParserBundle:UserData')
                ->findOneById(UserData::SUBSCRIPTION_ID);
            
            $em->getRepository('RealEstateParserBundle:UserDataState')
                ->setChangedState($user, $userData);
        }
    }
}
