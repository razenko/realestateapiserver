<?php

namespace RealEstate\ParserBundle\EventListener;

use RealEstate\ParserBundle\Controller\ApiController;
use RealEstate\ParserBundle\Controller\AdvertisementController;
use RealEstate\ParserBundle\Controller\UserController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use RealEstate\ParserBundle\Entity\Realty as Realty;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ApiRequestListener
{
    protected $request;
    protected $container;
    protected $event;
    protected $router;
    
    
    
    public function __construct($router, ContainerInterface $container) {
        $this->router = $router;
        $this->container = $container;
    }
    
    public function setRequest(RequestStack $request_stack)
    {
        $this->request = $request_stack->getCurrentRequest();
    }
    
    public function onKernelController(FilterControllerEvent $event)
    {
    	$this->event = $event;
        $controller = $event->getController();
        
        header('Access-Control-Allow-Origin: *');
        
        /*
         * $controller passed can be either a class or a Closure. This is not usual in Symfony2 but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller))
            return;
        
        $controller = $controller[0];
        
        /*if ($controller instanceof AdvertisementController)
            $this->validateAdvertisement();
        else */
        if ($controller instanceof UserController)
            $this->validateUser();
        else if ($controller instanceof ConfigurationController)
            $this->validateApplication();
    }
    
    public function validateAdvertisement() 
    {
    	$this->validateAppToken();
    	$this->validateAuthToken();
    	
    	$params = ($this->request->request->get('filter')) ? $this->request->request->get('filter') : $this->request->query->get('filter');
    	$decoded_params = json_decode($params, true);
    	
    	$this->validateTableFields($decoded_params);
    }
    
    public function validateUser() 
    {
    	$this->validateAppToken();
    }
    
    public function validateApplication()
    {
    	$this->validateAppToken();
    	$this->validateAuthToken();
    }
    
    public function validateAppToken()
    {
    	$request_app_token = ($this->request->request->get('app_token')) ? $this->request->request->get('app_token') : $this->request->query->get('app_token');
    	$app_token = $this->container->getParameter('app_token');
    	 
    	if(!$request_app_token)
            $this->sendError('app_token is required');
        
    	if((int)$request_app_token !== $app_token)
            $this->sendError('app_token doesn\'t match applications app_token');
    }
    
    public function validateAuthToken()
    {
    	$request_auth_token = ($this->request->request->get('auth_token')) ? $this->request->request->get('auth_token') : $this->request->query->get('auth_token');

    	if(!$request_auth_token)
            $this->sendError('auth_token is required');
        
//     	if((int)$request_auth_token !== (int)$auth_token)
//          $this->sendError('auth_token doesn\'t match applications auth_token');
    }
    
    public function validateTableFields( $params )
    {
    	$realty = new Realty();
        
    	foreach($params['filters'] as $filter_params)
        {
            if ( isset($filter_params['field']) ) {
                if (!method_exists($realty , 'get' . $this->toCamelCase($filter_params['field'])))
                    $this->sendError('field ' . $filter_params['field'] . ' does\'t exist in database.');
            }
            else
            {
                if(isset($filter_params['filters']))
                    $this->validateTableFields( $filter_params );
            }
    	}
    }
    
    public function sendError($message)
    {
    	$error_rout = $this->router->generate('error', array('message' => $message));
    	$this->event->setController(function() use ($error_rout) {
            return new RedirectResponse($error_rout);
    	});
    }
    
    public function toCamelCase($str, $capitalise_first_char = false) 
    {
    	if($capitalise_first_char)
            $str[0] = strtoupper($str[0]);
        
    	$func = create_function('$c', 'return strtoupper($c[1]);');
        
    	return preg_replace_callback('/_([a-z])/', $func, $str);
    }
}