<?php

require_once 'autoload.php';


define('START_USER_LOGIN', 70000000010);

// переносим параметры из командной строки в $_GET
parse_str(implode('&', array_slice($argv, 1)), $_GET);
$users_count = isset($_GET['users_count']) ? $_GET['users_count'] : null;

if (is_null($users_count))
{
    echo "Error! Users count limit is not set" . PHP_EOL;
    echo "example: php overloadTest.php users_count=2" . PHP_EOL;
    exit;
}

if (!file_exists("/tmp/realestate"))
    mkdir("/tmp/realestate", 0777);

for ($i = 0; $i < $users_count; $i++)
{
    $phone = START_USER_LOGIN + $i;
    
    shell_exec("php -f " . __DIR__ . "/userSetupActivityTest.php phone=$phone >> /tmp/realestate/{$phone}_bench.log &");
}

exit;