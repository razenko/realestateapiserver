<?php

require_once 'autoload.php';


define('TEST_USER_PASSWORD', '1010');
define('API_HOST', 'http://54.76.58.228');

define('FILTER_JSON', '{"deal_type":1,"term":0,"min_price":281,"region_id":1,'
. '"roomsCount":[0,1,2,3,4],"metroStationRegions":[1,2],"metroStations":[{"id":"53","name":"Александровский сад"},'
. '{"id":"50","name":"Арбатская"},{"id":"71","name":"Баррикадная"},{"id":"7","name":"Белорусская"},'
. '{"id":"30","name":"Библиотека им. Ленина"},{"id":"120","name":"Боровицкая"},'
. '{"id":"132","name":"Добрынинская"},{"id":"52","name":"Киевская"},{"id":"74","name":"Китай-город"},'
. '{"id":"35","name":"Комсомольская"},{"id":"133","name":"Краснопресненская"},{"id":"34","name":"Красные Ворота"},'
. '{"id":"29","name":"Кропоткинская"},{"id":"73","name":"Кузнецкий Мост"},{"id":"48","name":"Курская"},'
. '{"id":"32","name":"Лубянка"},{"id":"87","name":"Марксистская"},{"id":"8","name":"Маяковская"},'
. '{"id":"117","name":"Менделеевская"},{"id":"11","name":"Новокузнецкая"},{"id":"134","name":"Новослободская"},'
. '{"id":"100","name":"Октябрьская"},{"id":"31","name":"Охотный Ряд"},{"id":"12","name":"Павелецкая"},'
. '{"id":"28","name":"Парк культуры"},{"id":"49","name":"Пл. Революции"},{"id":"121","name":"Полянка"},'
. '{"id":"136","name":"Проспект Мира"},{"id":"72","name":"Пушкинская"},{"id":"122","name":"Серпуховская"},'
. '{"id":"51","name":"Смоленская"},{"id":"206","name":"Сретенский бульвар"},{"id":"102","name":"Сухаревская"},'
. '{"id":"75","name":"Таганская"},{"id":"9","name":"Тверская"},{"id":"10","name":"Театральная"},'
. '{"id":"88","name":"Третьяковская"},{"id":"199","name":"Трубная"},{"id":"103","name":"Тургеневская"},'
. '{"id":"118","name":"Цветной бульвар"},{"id":"119","name":"Чеховская"},{"id":"33","name":"Чистые пруды"},'
. '{"id":"137","name":"Чкаловская"}],"max_price":88581,"furniture":1,"kitchen_furniture":1,"telephone":0,"tv":0,'
. '"refrigerator":0,"washing_machine":0,"balcony":1,"pets":0,"children":0}');

define('REALTY_FILTER_JSON', '{"logic":"and","filters":[{"field":"deal_type","operator":"eq","value":1},'
. '{"field":"price","operator":"gte","value":281},{"field":"price","operator":"lte","value":88581},'
. '{"logic":"or","filters":[{"field":"rooms_count","operator":"eq","value":0},'
. '{"field":"rooms_count","operator":"eq","value":1},{"field":"rooms_count","operator":"eq","value":2},'
. '{"field":"rooms_count","operator":"eq","value":3},{"field":"rooms_count","operator":"eq","value":4}]},'
. '{"logic":"or","filters":[{"field":"metro","operator":"eq","value":"Александровский сад"},'
. '{"field":"metro","operator":"eq","value":"Арбатская"},{"field":"metro","operator":"eq","value":"Баррикадная"},'
. '{"field":"metro","operator":"eq","value":"Белорусская"},{"field":"metro","operator":"eq","value":"Библиотека им. Ленина"},'
. '{"field":"metro","operator":"eq","value":"Боровицкая"},{"field":"metro","operator":"eq","value":"Добрынинская"},'
. '{"field":"metro","operator":"eq","value":"Киевская"},{"field":"metro","operator":"eq","value":"Китай-город"},'
. '{"field":"metro","operator":"eq","value":"Комсомольская"},{"field":"metro","operator":"eq","value":"Краснопресненская"},'
. '{"field":"metro","operator":"eq","value":"Красные Ворота"},{"field":"metro","operator":"eq","value":"Кропоткинская"},'
. '{"field":"metro","operator":"eq","value":"Кузнецкий Мост"},{"field":"metro","operator":"eq","value":"Курская"},'
. '{"field":"metro","operator":"eq","value":"Лубянка"},{"field":"metro","operator":"eq","value":"Марксистская"},'
. '{"field":"metro","operator":"eq","value":"Маяковская"},{"field":"metro","operator":"eq","value":"Менделеевская"},'
. '{"field":"metro","operator":"eq","value":"Новокузнецкая"},{"field":"metro","operator":"eq","value":"Новослободская"},'
. '{"field":"metro","operator":"eq","value":"Октябрьская"},{"field":"metro","operator":"eq","value":"Охотный Ряд"},'
. '{"field":"metro","operator":"eq","value":"Павелецкая"},{"field":"metro","operator":"eq","value":"Парк культуры"},'
. '{"field":"metro","operator":"eq","value":"Пл. Революции"},{"field":"metro","operator":"eq","value":"Полянка"},'
. '{"field":"metro","operator":"eq","value":"Проспект Мира"},{"field":"metro","operator":"eq","value":"Пушкинская"},'
. '{"field":"metro","operator":"eq","value":"Серпуховская"},{"field":"metro","operator":"eq","value":"Смоленская"},'
. '{"field":"metro","operator":"eq","value":"Сретенский бульвар"},{"field":"metro","operator":"eq","value":"Сухаревская"},'
. '{"field":"metro","operator":"eq","value":"Таганская"},{"field":"metro","operator":"eq","value":"Тверская"},'
. '{"field":"metro","operator":"eq","value":"Театральная"},{"field":"metro","operator":"eq","value":"Третьяковская"},'
. '{"field":"metro","operator":"eq","value":"Трубная"},{"field":"metro","operator":"eq","value":"Тургеневская"},'
. '{"field":"metro","operator":"eq","value":"Цветной бульвар"},{"field":"metro","operator":"eq","value":"Чеховская"},'
. '{"field":"metro","operator":"eq","value":"Чистые пруды"},{"field":"metro","operator":"eq","value":"Чкаловская"}]},'
. '{"field":"region_id","operator":"eq","value":1},{"field":"furniture","operator":"eq","value":true},'
. '{"field":"kitchen_furniture","operator":"eq","value":true},{"field":"balcony","operator":"eq","value":true},'
. '{"logic":"or","filters":[{"field":"updated_at_date","operator":"gt","value":"2014-08-01"},'
. '{"logic":"and","filters":[{"field":"updated_at_date","operator":"eq","value":"2014-08-01"},'
. '{"field":"updated_at_time","operator":"gte","value":"00:00:00"}]}]}]}');




// переносим параметры из командной строки в $_GET
parse_str(implode('&', array_slice($argv, 1)), $_GET);
$phone = isset($_GET['phone']) ? $_GET['phone'] : null;

if (is_null($phone))
{
    echo "Error! Phone number is not set" . PHP_EOL;
    echo "example: php userSetupActivityTest.php phone=70000000010" . PHP_EOL;
    exit;
}

$userActivity = array(
    'preRegistration' => array(
        'url' => '/user/send-login-sms',
        'request' => array(
            'phone' => $phone
        )
    ),
    'registration'=> array(
        'url' => '/user/confirm-code',
        'request' => array(
            'phone' => $phone,
            'code' => TEST_USER_PASSWORD
        ),
        'response' => array(
            'auth_token' => array(
                array(
                    'action' => 'saveFilter',
                    'request_parameter' => 'auth_token'
                ),
                array(
                    'action' => 'getAdvertisements',
                    'request_parameter' => 'auth_token'
                )
            )
        )
    ),
    'saveFilter' => array(
        'url' => '/user/save-filter',
        'request' => array(
            'auth_token' => 'from response',
            'filter' => FILTER_JSON
        )
    ),
    'getAdvertisements' => array(
        'url' => '/advertisement/get',
        'request' => array(
            'auth_token' => 'from response',
            'filter' => REALTY_FILTER_JSON
        )
    ),
);

$commonRequestFields = array(
    //'XDEBUG_SESSION_START' => 'netbeans-xdebug',
    'app_token' => '12345'
);


$total_start_microtime = microtime(true);

try
{
    foreach ($userActivity as $action_name => &$action)
    {
        $start_microtime = microtime(true);
        
        $requestParams = $action['request'];
        $requestParams = array_merge($commonRequestFields, $requestParams);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, API_HOST . $action['url']); // Set URL to GCM endpoint
        curl_setopt($ch, CURLOPT_POST, true); // Set request method to POST
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Set request method to POST
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Get the response back as string instead of printing it
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($requestParams)); // Set post data as JSON
        
        $response = curl_exec($ch);
        curl_close($ch);
        
        $time = microtime(true) - $start_microtime;
        $time *= 1000;
        echo "User +$phone finished $action_name in $time miliseconds" . PHP_EOL;
        
        if (!$response)
            throw new Exception("Error on action '$action_name'");
        
        $response = json_decode($response, true);
        
        if (isset($response['error']))
        {
            var_dump($response['error']);
            echo PHP_EOL;
        }
        
        if (empty($action['response']))
            continue;
        
        foreach ($action['response'] as $parameter_name => $actionSettingsArray)
        {
            if (!isset($response['data'][$parameter_name]))
                continue;
            
            foreach ($actionSettingsArray as $actionSettings)
            {
                $setup_action_name = $actionSettings['action'];
                $setup_parameter_name = $actionSettings['request_parameter'];
                $value = $response['data'][$parameter_name];
                $userActivity[$setup_action_name]['request'][$setup_parameter_name] = $value;
            }
        }
    }
}
catch (Exception $exc)
{
    echo $exc->getMessage() . PHP_EOL;
    exit;
}

$total_time = microtime(true) - $total_start_microtime;
$total_time *= 1000;
echo "User +$phone prepared successfully (total time) in $total_time miliseconds" . PHP_EOL;

echo PHP_EOL;
echo "*************************************************************************************";
echo PHP_EOL . PHP_EOL;

exit;