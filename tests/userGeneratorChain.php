<?php

require_once 'autoload.php';


define('START_USER_LOGIN', 70000013000);

// переносим параметры из командной строки в $_GET
parse_str(implode('&', array_slice($argv, 1)), $_GET);
$users_count = isset($_GET['users_count']) ? $_GET['users_count'] : null;
$start_user_login = isset($_GET['start_user_login']) ? $_GET['start_user_login'] : START_USER_LOGIN;

if (is_null($users_count))
{
    echo "Error! Users count limit is not set" . PHP_EOL;
    echo "example: php userGeneratorChain.php users_count=2" . PHP_EOL;
    exit;
}

if (!$start_user_login)
{
    echo "Error! Start user login is not set" . PHP_EOL;
    echo "example: php userGeneratorChain.php users_count=2 start_user_login=70000000010" . PHP_EOL;
    exit;
}

if (!file_exists("/tmp/realestate"))
    mkdir("/tmp/realestate", 0777);

if (!file_exists("/tmp/realestate/generator"))
    mkdir("/tmp/realestate/generator", 0777);

for ($i = 0; $i < $users_count; $i++)
{
    $phone = $start_user_login + $i;
    
    shell_exec("php " . __DIR__ . "/userGenerator.php phone=$phone > /tmp/realestate/generator/{$phone}.log");
    
    $generated_users_count = $i + 1;
    echo "There are {$generated_users_count} of {$users_count} users generated" . PHP_EOL;
}

exit;
